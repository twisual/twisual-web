package com.fszuberski.twisual.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import twitter4j.*;

import java.util.Date;

@Data
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class TwitterStatus {

    @Id
    private String id;
    private Date createdAt;
    private long twitter_id;
    private String text;
    @Builder.Default
    private int displayTextRangeStart = -1;
    @Builder.Default
    private int displayTextRangeEnd = -1;
    private String source;
    private boolean isTruncated;
    private long inReplyToStatusId;
    private long inReplyToUserId;
    private boolean isFavorited;
    private boolean isRetweeted;
    private int favoriteCount;
    private String inReplyToScreenName;
    @Builder.Default
    private twitter4j.GeoLocation geoLocation = null;
    @Builder.Default
    private Place place = null;
    private long retweetCount;
    private boolean isPossiblySensitive;
    private String lang;
    private long[] contributorsIDs;
    private Status retweetedStatus;
    private UserMentionEntity[] userMentionEntities;
    private URLEntity[] urlEntities;
    private HashtagEntity[] hashtagEntities;
    private MediaEntity[] mediaEntities;
    private SymbolEntity[] symbolEntities;
    @Builder.Default
    private long currentUserRetweetId = -1L;
    private Scopes scopes;
    @Builder.Default
    private User user = null;
    @Builder.Default
    private String[] withheldInCountries = null;
    private Status quotedStatus;
    @Builder.Default
    private long quotedStatusId = -1L;
}
