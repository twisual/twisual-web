package com.fszuberski.twisual.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeoLocation {
    private Double latitude;
    private Double longitude;

    public GeoLocation(twitter4j.GeoLocation twitterGeoLocation) {
        this.latitude = twitterGeoLocation.getLatitude();
        this.longitude = twitterGeoLocation.getLongitude();
    }
}
