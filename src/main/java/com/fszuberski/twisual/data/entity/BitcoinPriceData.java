package com.fszuberski.twisual.data.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
public class BitcoinPriceData {

    @Id
    private String id;
    private Double high;
    private Double low;
    private Double open;
    private Double average;
    private Date time;
    private Double volume;
    private Integer popularity;
}
