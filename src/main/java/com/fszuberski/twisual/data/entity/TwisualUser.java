package com.fszuberski.twisual.data.entity;

import com.fszuberski.twisual.common.enums.AccountStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class TwisualUser {

    @Id
    private String id;
    private Date registeredAt;
    private String login;
    private String email;
    private String passwordHash;
    private String passwordSalt;
    private int sessionLength;
    private int accountStatus;

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus.getId();
    }
}
