package com.fszuberski.twisual.data.entity;

import com.fszuberski.twisual.stream.entity.Place;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.*;

@Data
@Builder
@AllArgsConstructor
public class TwitterStatusStatistics {

    @Id
    private String id;

    private Date createdAtStartDate;
    private Date createdAtEndDate;
    private int dayOfWeek;
    private int timeType;

    private int tweetCount;
    private int truncatedCount;
    private int favoritedCount;
    private int retweetedCount;
    private int possiblySensitiveCount;
    private int userMentionEntityCount;
    private int urlEntityCount;
    private int hashtagEntityCount;
    private int mediaEntityCount;
    private int symbolEntityCount;
    private List<GeoLocation> geoLocations;
    private Map<String, Integer> sources;
    private Map<String, Integer> languages;
    private Map<String, Integer> userMentionEntities;
    private Map<String, Integer> urlEntities;
    private Map<String, Integer> hashtagEntities;
    private Map<String, Integer> mediaEntityTypes;
    private Map<String, Integer> mediaEntityUrls;
    private Map<String, Integer> symbolEntityTexts;
    private Map<String, Integer> placeTypes;
    private List<Place> places;
    private Map<String, HashMap<String, Integer>> words;

    public TwitterStatusStatistics() {
        this.tweetCount = 0;
        this.truncatedCount = 0;
        this.favoritedCount = 0;
        this.retweetedCount = 0;
        this.possiblySensitiveCount = 0;
        this.userMentionEntityCount = 0;
        this.urlEntityCount = 0;
        this.hashtagEntityCount = 0;
        this.mediaEntityCount = 0;
        this.symbolEntityCount = 0;
        this.geoLocations = new ArrayList<>();
        this.sources = new HashMap<>();
        this.languages = new HashMap<>();
        this.userMentionEntities = new HashMap<>();
        this.urlEntities = new HashMap<>();
        this.hashtagEntities = new HashMap<>();
        this.mediaEntityTypes = new HashMap<>();
        this.mediaEntityUrls = new HashMap<>();
        this.symbolEntityTexts = new HashMap<>();
        this.placeTypes = new HashMap<>();
        this.places = new ArrayList<>();
        this.words = new HashMap<>();
    }

    public TwitterStatusStatistics(Date createdAtStartDate, Date createdAtEndDate) {
        this.createdAtStartDate = createdAtStartDate;
        this.createdAtEndDate = createdAtEndDate;

        this.tweetCount = 0;
        this.truncatedCount = 0;
        this.favoritedCount = 0;
        this.retweetedCount = 0;
        this.possiblySensitiveCount = 0;
        this.userMentionEntityCount = 0;
        this.urlEntityCount = 0;
        this.hashtagEntityCount = 0;
        this.mediaEntityCount = 0;
        this.symbolEntityCount = 0;
        this.geoLocations = new ArrayList<>();
        this.sources = new HashMap<>();
        this.languages = new HashMap<>();
        this.userMentionEntities = new HashMap<>();
        this.urlEntities = new HashMap<>();
        this.hashtagEntities = new HashMap<>();
        this.mediaEntityTypes = new HashMap<>();
        this.mediaEntityUrls = new HashMap<>();
        this.symbolEntityTexts = new HashMap<>();
        this.placeTypes = new HashMap<>();
        this.places = new ArrayList<>();
        this.words = new HashMap<>();
    }
}
