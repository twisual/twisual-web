package com.fszuberski.twisual.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.*;

@Data
@Builder
@AllArgsConstructor
public class TwitterUserStatistics {

    @Id
    private String id;

    private Date createdAtStartDate;
    private Date createdAtEndDate;
    private int dayOfWeek;
    private int timeType;

    private int userCount;
    private int nameCount;
    private int emailCount;
    private int screenNameCount;
    private int descriptionUrlCount;
    private int contributorsEnabledCount;
    private int profileImageUrlCount;
    private int defaultProfileImageCount;
    private int urlCount;
    private int protectedCount;
    private int followersCount;
    private int profileUseBackgroundImageCount;
    private int defaultProfileCount;
    private int showAllInlineMediaCount;
    private int friendsCount;
    private int profileBackgroundImageUrlCount;
    private int profileBannerImageUrlCount;
    private int profileBackgroundTiledCount;
    private int geoEnabledCount;
    private int verifiedCount;
    private int statusesCount;

    private List<Date> createdAt;
    private Map<String, Integer> emails;
    private Map<String, Integer> locations;
    private Map<String, Integer> descriptionUrls;
    private Map<String, Integer> profileImageUrls;
    private Map<String, Integer> urls;
    private Map<String, Integer> languages;
    private Map<String, Integer> profileBackgroundColors;
    private Map<String, Integer> profileTextColors;
    private Map<String, Integer> profileLinkColors;
    private Map<String, Integer> profileSidebarFillColors;
    private Map<String, Integer> profileSidebarBorderColors;
    private Map<String, Integer> profileBackgroundImageUrls;
    private Map<Integer, Integer> utcOffsets;
    private Map<String, Integer> timeZones;
    private Map<String, HashMap<String, Integer>> descriptionWords;

    public TwitterUserStatistics() {
        this.userCount = 0;
        this.nameCount = 0;
        this.emailCount = 0;
        this.screenNameCount = 0;
        this.descriptionUrlCount = 0;
        this.contributorsEnabledCount = 0;
        this.profileImageUrlCount = 0;
        this.defaultProfileCount = 0;
        this.urlCount = 0;
        this.protectedCount = 0;
        this.followersCount = 0;
        this.profileUseBackgroundImageCount = 0;
        this.defaultProfileCount = 0;
        this.showAllInlineMediaCount = 0;
        this.friendsCount = 0;
        this.profileBackgroundImageUrlCount = 0;
        this.profileBannerImageUrlCount = 0;
        this.profileBackgroundTiledCount = 0;
        this.geoEnabledCount = 0;
        this.verifiedCount = 0;
        this.statusesCount = 0;

        this.createdAt = new ArrayList<>();
        this.emails = new HashMap<>();
        this.locations = new HashMap<>();
        this.descriptionUrls = new HashMap<>();
        this.profileImageUrls = new HashMap<>();
        this.urls = new HashMap<>();
        this.languages = new HashMap<>();
        this.profileBackgroundColors = new HashMap<>();
        this.profileTextColors = new HashMap<>();
        this.profileLinkColors = new HashMap<>();
        this.profileSidebarFillColors = new HashMap<>();
        this.profileSidebarBorderColors = new HashMap<>();
        this.profileBackgroundImageUrls = new HashMap<>();
        this.utcOffsets = new HashMap<>();
        this.timeZones = new HashMap<>();
        this.descriptionWords = new HashMap<>();

    }
}
