package com.fszuberski.twisual.data.boundary;

import com.fszuberski.twisual.data.entity.TwitterStatusStatistics;
import io.reactivex.Observable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;
import twitter4j.Twitter;

import java.util.Date;

@Repository
public interface TwitterStatusStatisticsRepository extends RxJava2CrudRepository<TwitterStatusStatistics, String> {

//    @Query(value = "{'createdAtStartDate': {'$gte': ?0}, 'createdAtEndDate': {'$lte': ?1}, 'timeType': {'$eq': 3}}", fields = { geoLocations: 1, _id: 0 }")
    @Query(value = "{'createdAtStartDate': {'$gte': ?0}, 'createdAtEndDate': {'$lte': ?1}}", fields = "{ geoLocations: 1, _id: 0 }")
    Observable<TwitterStatusStatistics> geoLocationData(Date startDate, Date endDate);

    @Query(value = "{'createdAtStartDate': {'$gte': ?0}, 'createdAtEndDate': {'$lte': ?1}, 'timeType': {'$eq': 3}}", fields = "{ words: 1, _id: 0 }")
    Observable<TwitterStatusStatistics> getWordTreeData(Date startDate, Date endDate);

//    @Query(value = "{'createdAtStartDate': {'$gte': ?0}, 'createdAtEndDate': {'$lte': ?1}, 'timeType': {'$eq': 3}}", fields = "{ words: 1, _id: 0 }")
    @Query(value = "{'createdAtStartDate': {'$gte': ?0}, 'createdAtEndDate': {'$lte': ?1}, 'timeType': {'$eq': 3}}", fields = "{ hashtagEntities: 1, _id: 0 }")
    Observable<TwitterStatusStatistics> bitcoinPopularityData(Date startDate, Date endDate);

    @Query(value = "{'createdAtStartDate': {'$gte': ?0}, 'createdAtEndDate': {'$lte': ?1}, 'timeType': {'$eq': 3}}", fields = "{ tweetCount: 1, _id: 0 }")
    Observable<TwitterStatusStatistics> tweetVolumeData(Date startDate, Date endDate);
}
