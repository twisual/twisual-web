package com.fszuberski.twisual.data.boundary;

import com.fszuberski.twisual.data.entity.TwisualUser;
import io.reactivex.Maybe;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwisualUserRepository extends RxJava2CrudRepository<TwisualUser, String> {

    Maybe<TwisualUser> findByLogin(String login);
}
