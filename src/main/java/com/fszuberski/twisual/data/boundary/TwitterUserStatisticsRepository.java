package com.fszuberski.twisual.data.boundary;

import com.fszuberski.twisual.data.entity.TwitterUserStatistics;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwitterUserStatisticsRepository extends RxJava2CrudRepository<TwitterUserStatistics, String> {

}
