package com.fszuberski.twisual.data.boundary;

import com.fszuberski.twisual.data.entity.BitcoinPriceData;
import io.reactivex.Observable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface BitcoinPriceDataRepository extends RxJava2CrudRepository<BitcoinPriceData, String> {

//    @Query(value = "{ $query: {'time': {'$gte': ?0}, 'time': {'$lt': ?1}}, $orderby: { 'time': 1 }}", fields = "{ time: 1, average: 1, _id: 0 }")
    @Query(value = "{ 'time': { '$gte': ?0, '$lt': ?1}}")
    Observable<BitcoinPriceData> findBetweenDates(Date startDate, Date endDate);
}
