package com.fszuberski.twisual.data.boundary;

import com.fszuberski.twisual.data.entity.TwitterUser;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

@SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
@Repository
public interface TwitterUserRepository extends RxJava2CrudRepository<TwitterUser, String> {

}
