package com.fszuberski.twisual.data.control;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import twitter4j.GeoLocation;

import java.util.LinkedHashMap;

@Slf4j
@Component
public class LinkedHashMapToGeoLocationConverter implements Converter<LinkedHashMap<?, ?>, GeoLocation> {

    @Override
    public GeoLocation convert(LinkedHashMap<?, ?> source) {
        Double latitude = (Double) source.get("latitude");
        Double longitude = (Double) source.get("longitude");

        return new GeoLocation(latitude, longitude);
    }
}
