package com.fszuberski.twisual.api.v1.filter.hmac;

public class HmacHeaders
{
    public static final String HEADER_X_TWISUAL_TIMESTAMP = "X-TWISUAL-TIMESTAMP";
    public static final String HEADER_X_TWISUAL_AUTHORIZATION = "X-TWISUAL-AUTHORIZATION";
    public static final String HEADER_X_TWISUAL_SIGNATURE = "X-TWISUAL-SIGNATURE";

}
