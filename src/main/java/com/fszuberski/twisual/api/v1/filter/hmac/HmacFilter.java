package com.fszuberski.twisual.api.v1.filter.hmac;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fszuberski.twisual.api.v1.security.hmac.HmacManager;
import com.fszuberski.twisual.common.manager.PropertiesManager;
import com.fszuberski.twisual.common.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.io.IOException;
import java.util.Date;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;
import static com.fszuberski.twisual.api.v1.filter.hmac.HmacHeaders.HEADER_X_TWISUAL_AUTHORIZATION;
import static com.fszuberski.twisual.api.v1.filter.hmac.HmacHeaders.HEADER_X_TWISUAL_SIGNATURE;
import static com.fszuberski.twisual.api.v1.filter.hmac.HmacHeaders.HEADER_X_TWISUAL_TIMESTAMP;

@Slf4j
@Component
public class HmacFilter implements ContainerRequestFilter, ContainerResponseFilter
{
    @Autowired
    HmacManager hmacManager;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    PropertiesManager propertiesManager;

    @Override
    public void filter(ContainerRequestContext requestContext)
    {
        // retrieving headers and body from request
        String httpMethod = requestContext.getMethod();
        String timestamp = requestContext.getHeaders().getFirst(HEADER_X_TWISUAL_TIMESTAMP);
        String publicKey = requestContext.getHeaders().getFirst(HEADER_X_TWISUAL_AUTHORIZATION);
        String signature = requestContext.getHeaders().getFirst(HEADER_X_TWISUAL_SIGNATURE);
        String body = StringUtils.readRequestBody(requestContext);

        hmacManager.compareSignatures(publicKey, signature, httpMethod + timestamp + body);
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException
    {
        // timestamp header
        long timestamp = new Date().getTime();
        responseContext.getHeaders().add(HEADER_X_TWISUAL_TIMESTAMP, timestamp);
        log.debug("Added response header: {}:{}", HEADER_X_TWISUAL_TIMESTAMP, timestamp);

        // signature header
        String publicKey = requestContext.getHeaders().getFirst(HEADER_X_TWISUAL_AUTHORIZATION);

        String body = responseContext.getEntity() != null ? objectMapper.writeValueAsString(responseContext.getEntity()) : "";

        String data = requestContext.getMethod() + timestamp + body;

        String signature = null;
        try
        {
            signature = publicKey != null ? hmacManager.generateSignature(publicKey, data) : null;
        } catch (Exception e)
        {
            log.warn("{} Exception while generating signature for response", API_TAG, e);
        }

        responseContext.getHeaders().add(HEADER_X_TWISUAL_SIGNATURE, signature);
        log.debug("Added response header: {}:{}", HEADER_X_TWISUAL_SIGNATURE, signature);
    }
}
