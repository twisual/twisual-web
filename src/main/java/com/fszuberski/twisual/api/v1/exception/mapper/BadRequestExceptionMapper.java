package com.fszuberski.twisual.api.v1.exception.mapper;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@Slf4j
public class BadRequestExceptionMapper implements ExceptionMapper<BadRequestException>
{
    @Override
    public Response toResponse(BadRequestException exception)
    {
        log.debug("{} {} has been invoked", API_TAG, this.getClass().getSimpleName());
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(exception.getMessage())
                .build();
    }
}
