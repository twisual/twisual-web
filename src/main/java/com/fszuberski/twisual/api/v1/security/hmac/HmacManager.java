package com.fszuberski.twisual.api.v1.security.hmac;

import com.fszuberski.twisual.api.v1.session.SessionInfo;
import com.fszuberski.twisual.api.v1.session.SessionManager;
import com.fszuberski.twisual.common.util.HashUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import java.util.Arrays;
import java.util.Base64;

import static com.fszuberski.twisual.api.v1.Constants.*;
import static com.fszuberski.twisual.common.util.StringUtils.randomString;
import static java.util.UUID.randomUUID;

@Slf4j
@Service
public class HmacManager {
    private SessionManager sessionManager;

    @Autowired
    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public HmacInfo generateKeyPair() {
        return HmacInfo
                .builder()
                .publicKey(HashUtils.sha256(randomUUID().toString()))
                .privateKey(HashUtils.sha256(Arrays.asList(randomString(256), randomUUID().toString(), String.valueOf(System.currentTimeMillis()))))
                .build();
    }

    public void compareSignatures(String publicKey, String signature, String stringedData) {
        String privateKey = retrievePrivateKey(publicKey);

        log.info("::compare signatures invoked: publicKey={}, privateKey={}", publicKey, privateKey);

        if (privateKey == null) {
            log.warn("{} HMAC key not authorized: {}", API_TAG, publicKey);
            throw new NotAuthorizedException(HEADER_WWW_AUTHENTICATE_AUTHORIZATION_MISSING_OR_INVALID);
        }

        String generatedSignature;
        try {
            generatedSignature = stringedData != null ? generateSignature(privateKey, stringedData) : null;

        } catch (Exception e) {
            log.warn("{} Exception while generating HMAC signature", API_TAG, e);
            throw new InternalServerErrorException();
        }

        if (!signature.equals(generatedSignature)) {
            log.warn("{} Generated signature does not match sent signature: generated={}, sent={}",
                    API_TAG, generatedSignature, signature);
            throw new NotAuthorizedException(HEADER_WWW_AUTHENTICATE_SIGNATURE_MISSING_OR_INVALID);
        }
    }

    public String generateSignature(String privateKey, String data) {
        String hash = privateKey != null ? HashUtils.sha256(privateKey + data) : "";
        return Base64.getEncoder().encodeToString(hash.getBytes());
    }


    protected String retrievePrivateKey(String publicKey) {
        SessionInfo sessionInfo = sessionManager.find(publicKey);
        return sessionInfo != null ? sessionInfo.getHmacPrivate() : null ;
    }
}
