package com.fszuberski.twisual.api.v1.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginRequestModel {

    @NotNull(message = "username cannot be null")
    @Size(message = "username cannot be empty", min = 1)
    private String username;

    @NotNull(message = "password cannot be null")
    @Size(message = "password cannot be null", min = 1)
    private String password;
}
