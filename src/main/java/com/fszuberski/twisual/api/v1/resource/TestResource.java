package com.fszuberski.twisual.api.v1.resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@Slf4j
@Component
@Path("test")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TestResource {

    @GET
    public Response test()
    {
        log.info("{} ::test invoked", API_TAG);
        return Response
                .status(Response.Status.OK)
                .entity(API_TAG + " is working!")
                .build();
    }
}
