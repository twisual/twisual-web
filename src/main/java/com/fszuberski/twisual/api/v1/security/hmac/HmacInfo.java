package com.fszuberski.twisual.api.v1.security.hmac;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HmacInfo
{
    private String publicKey;
    private String privateKey;
}
