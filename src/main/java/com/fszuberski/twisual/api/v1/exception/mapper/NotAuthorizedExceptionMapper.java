package com.fszuberski.twisual.api.v1.exception.mapper;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;
import static javax.ws.rs.core.HttpHeaders.WWW_AUTHENTICATE;
import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
public class NotAuthorizedExceptionMapper implements ExceptionMapper<NotAuthorizedException>
{
    @Override
    public Response toResponse(NotAuthorizedException exception)
    {
        log.debug("{} {} has been invoked", API_TAG, this.getClass().getSimpleName());

        Response.ResponseBuilder responseBuilder = Response
                .status(Response.Status.UNAUTHORIZED)
                .entity(exception.getMessage());

        String wwwAuthenticateHeader = exception.getResponse().getHeaderString(WWW_AUTHENTICATE);
        if (!isEmpty(wwwAuthenticateHeader))
        {
            responseBuilder.header(WWW_AUTHENTICATE, wwwAuthenticateHeader);
        }

        return responseBuilder.build();
    }
}
