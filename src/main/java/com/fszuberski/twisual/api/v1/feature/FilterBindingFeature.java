package com.fszuberski.twisual.api.v1.feature;

import com.fszuberski.twisual.api.v1.filter.AuthenticationFilter;
import com.fszuberski.twisual.api.v1.filter.hmac.HmacFilter;
import com.fszuberski.twisual.api.v1.security.AuthenticationRequired;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;

public class FilterBindingFeature implements DynamicFeature
{
    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context)
    {
        if (resourceInfo.getResourceMethod().isAnnotationPresent(AuthenticationRequired.class))
        {
            context.register(HmacFilter.class);
            context.register(AuthenticationFilter.class);
        }
    }
}
