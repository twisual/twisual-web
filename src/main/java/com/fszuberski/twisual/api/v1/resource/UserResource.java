package com.fszuberski.twisual.api.v1.resource;

import com.fszuberski.twisual.api.v1.model.UserLoginRequestModel;
import com.fszuberski.twisual.api.v1.model.UserLoginResponseModel;
import com.fszuberski.twisual.api.v1.security.AuthenticationRequired;
import com.fszuberski.twisual.api.v1.security.hmac.HmacInfo;
import com.fszuberski.twisual.api.v1.security.hmac.HmacManager;
import com.fszuberski.twisual.api.v1.session.SessionInfo;
import com.fszuberski.twisual.api.v1.session.SessionManager;
import com.fszuberski.twisual.business.boundary.TwisualUserFacade;
import com.fszuberski.twisual.data.entity.TwisualUser;
import io.reactivex.Maybe;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.Date;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;
import static com.fszuberski.twisual.api.v1.Constants.HEADER_WWW_AUTHENTICATE_WRONG_LOGIN_OR_PASSWORD;
import static com.fszuberski.twisual.api.v1.filter.hmac.HmacHeaders.HEADER_X_TWISUAL_AUTHORIZATION;
import static com.fszuberski.twisual.common.util.DateUtils.longFormat;

@Slf4j
@Component
@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    private TwisualUserFacade twisualUserFacade;
    private SessionManager sessionManager;
    private HmacManager hmacManager;

    @Autowired
    public void setTwisualUserFacade(TwisualUserFacade twisualUserFacade) {
        this.twisualUserFacade = twisualUserFacade;
    }

    @Autowired
    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    @Autowired
    public void setHmacManager(HmacManager hmacManager) {
        this.hmacManager = hmacManager;
    }

    @POST
    @Path("sign-in")
    public UserLoginResponseModel signIn(@Valid UserLoginRequestModel userLoginRequestModel) {
        log.info("{} ::signIn invoked with parameters: username={}, password=*****", API_TAG, userLoginRequestModel.getUsername());
        Maybe<TwisualUser> twisualUserMaybe = twisualUserFacade
                .findByLoginAndPassword(userLoginRequestModel.getUsername(), userLoginRequestModel.getPassword());

        TwisualUser twisualUser = twisualUserMaybe
                .blockingGet();

        if (twisualUser == null) {
            log.info("{} Invalid login credentials (login={})", API_TAG, userLoginRequestModel.getUsername());
            throw new NotAuthorizedException(HEADER_WWW_AUTHENTICATE_WRONG_LOGIN_OR_PASSWORD);
        }

        HmacInfo hmacInfo = hmacManager.generateKeyPair();

        SessionInfo sessionInfo = SessionInfo
                .builder()
                .twisualUser(twisualUser)
                .hmacPublic(hmacInfo.getPublicKey())
                .hmacPrivate(hmacInfo.getPrivateKey())
                .lastAction(new Date())
                .build();

        sessionManager.create(hmacInfo.getPublicKey(), sessionInfo);

        return UserLoginResponseModel
                .builder()
                .login(sessionInfo.getTwisualUser().getLogin())
                .hmacPublic(sessionInfo.getHmacPublic())
                .hmacPrivate(sessionInfo.getHmacPrivate())
                .lastAction(longFormat().format(sessionInfo.getLastAction()))
                .build();
    }

    @PUT
    @Path("sign-out")
    @AuthenticationRequired
    public void signOut(@HeaderParam(HEADER_X_TWISUAL_AUTHORIZATION) String authorizationHeader) {
        log.info("{} ::signOut invoked", API_TAG);
        sessionManager.invalidate(authorizationHeader);
    }
}