package com.fszuberski.twisual.api.v1.session;

import com.fszuberski.twisual.data.entity.TwisualUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessionInfo {
    private TwisualUser twisualUser;
    private String hmacPublic;
    private String hmacPrivate;
    private Date lastAction;
}
