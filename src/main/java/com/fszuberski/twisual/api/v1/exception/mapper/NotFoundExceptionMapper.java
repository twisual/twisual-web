package com.fszuberski.twisual.api.v1.exception.mapper;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@Slf4j
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException>
{
    @Override
    public Response toResponse(NotFoundException exception)
    {
        log.debug("{} {} has been invoked", API_TAG, this.getClass().getSimpleName());
        return Response
                .status(Response.Status.NOT_FOUND)
                .entity(exception.getMessage())
                .build();
    }
}
