package com.fszuberski.twisual.api.v1.session;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class SessionManager {

    @Getter
    @Setter
    private Map<String /* hmac public*/, SessionInfo> sessionMap;

    @PostConstruct
    public void init() {
        log.info("SessionManager initialized");
        sessionMap = new ConcurrentHashMap<>();
    }

    public SessionInfo find(String hmacPublic) {
        log.info("::find invoked with parameter: hmacPublic={}", hmacPublic);
        return sessionMap.get(hmacPublic);
    }

    public void create(String hmacPublic, SessionInfo sessionInfo) {
        log.trace("::create invoked with parameters: hmacPublic={}, sessionInfo={}", hmacPublic, sessionInfo);
        sessionMap.put(hmacPublic, sessionInfo);
    }

    public void update(String hmacPublic, SessionInfo sessionInfo) {
        log.trace("::update invoked with parameters: hmacPublic={}, sessionInfo={}", hmacPublic, sessionInfo);
        create(hmacPublic, sessionInfo);
    }

    public void invalidate(String hmacPublic) {
        log.trace("::invalidate invoked with parameters: hmacPublic={}", hmacPublic);
        sessionMap.remove(hmacPublic);
    }

    public void printPublicKeys() {
        log.info("------------------- SESSION PUBLIC KEYS -------------------");

        for (Map.Entry<String, SessionInfo> entry : sessionMap.entrySet()) {
            log.info("{}", entry.getKey());
        }

        log.info("------------------- END SESSION PUBLIC KEYS -------------------");
    }
}
