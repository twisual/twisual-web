package com.fszuberski.twisual.api.v1.exception.mapper;

import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.ArrayList;
import java.util.List;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@Slf4j
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException>
{
    @Override
    public Response toResponse(ConstraintViolationException exception)
    {
        log.debug("{} {} has been invoked", API_TAG, this.getClass().getSimpleName());
        List<String> constraintViolationMessages = new ArrayList<>();
        for (ConstraintViolation constraintViolation : exception.getConstraintViolations())
        {
            constraintViolationMessages.add(constraintViolation.getMessage());
        }

        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity(constraintViolationMessages)
                .build();
    }
}
