package com.fszuberski.twisual.api.v1.filter;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import java.io.IOException;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@Slf4j
public class RequestLoggingFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        log.info("{} ::filter invoked", API_TAG);
    }
}
