package com.fszuberski.twisual.api.v1.resource;

import com.fszuberski.twisual.api.v1.security.AuthenticationRequired;
import com.fszuberski.twisual.business.boundary.TweetDataFacade;
import com.fszuberski.twisual.business.entity.*;
import com.fszuberski.twisual.common.util.DateUtils;
import com.fszuberski.twisual.data.entity.GeoLocation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@SuppressWarnings("Duplicates")
@Slf4j
@Component
@Path("tweets")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TweetResource {

    private TweetDataFacade tweetDataFacade;

    @Autowired
    public void setTweetDataFacade(TweetDataFacade tweetDataFacade) {
        this.tweetDataFacade = tweetDataFacade;
    }

    @GET
    @Path("geo-location")
    @AuthenticationRequired
    public List<GeoLocation> geoLocationData(@NotNull(message = "startDate cannot be null")
                                             @Size(message = "startDate cannot be empty", min = 1)
                                             @QueryParam("startDate")
                                                     String startDate,

                                             @NotNull(message = "endDate cannot be null")
                                             @Size(message = "endDate cannot be empty", min = 1)
                                             @QueryParam("endDate")
                                                     String endDate) {
        log.info("{} ::geoLocationData invoked with parameters: startDate={}, endDate={}", API_TAG, startDate, endDate);
        long start = System.currentTimeMillis();

        List<GeoLocation> geoLocationList = new ArrayList<>();

        Date startDateObj;
        Date endDateObj;

        try {
            startDateObj = DateUtils.formatter.parse(startDate);
            endDateObj = DateUtils.formatter.parse(endDate);
        } catch (Exception e) {
            log.warn("{} Exception while parsing dates", API_TAG, e);
            throw new BadRequestException("Invalid date(s)");
        }

        tweetDataFacade
                .tweetGeoLocationData(startDateObj, endDateObj)
                .blockingForEach(geoLocationList::add);

        log.info("{} ::geoLocationData finished in {} ms", API_TAG, (System.currentTimeMillis() - start));
        return geoLocationList;
    }

    @GET
    @Path("word-tree")
    @AuthenticationRequired
    public List<WordTreeDataModel> getWordTreeData(@NotNull(message = "startDate cannot be null")
                                    @Size(message = "startDate cannot be empty", min = 1)
                                    @QueryParam("startDate")
                                            String startDate,

                                                   @NotNull(message = "endDate cannot be null")
                                    @Size(message = "endDate cannot be empty", min = 1)
                                    @QueryParam("endDate")
                                            String endDate) {
        log.info("{} ::getWordTreeData invoked with parameters: startDate={}, endDate={}", API_TAG, startDate, endDate);
        long start = System.currentTimeMillis();

        Date startDateObj;
        Date endDateObj;

        try {
            startDateObj = DateUtils.formatter.parse(startDate);
            endDateObj = DateUtils.formatter.parse(endDate);
        } catch (Exception e) {
            log.warn("{} Exception while parsing dates", API_TAG, e);
            throw new BadRequestException("Invalid date(s)");
        }

        WordTreeDataModel data = tweetDataFacade
                .getWordTreeData(startDateObj, endDateObj)
                .blockingGet();
        List<WordTreeDataModel> dataAsArray = new ArrayList<>();
        dataAsArray.add(data);

        log.info("{} ::getWordTreeData finished in {} ms", API_TAG, (System.currentTimeMillis() - start));
        return dataAsArray;
    }

    @GET
    @Path("bitcoin-price-popularity")
    @AuthenticationRequired
    public List<BitcoinPricePopularityDataModel> getBitcoinPriceAndPopularityData(@NotNull(message = "startDate cannot be null")
                                                                                  @Size(message = "startDate cannot be empty", min = 1)
                                                                                  @QueryParam("startDate")
                                                                                          String startDate,

                                                                                  @NotNull(message = "endDate cannot be null")
                                                                                  @Size(message = "endDate cannot be empty", min = 1)
                                                                                  @QueryParam("endDate")
                                                                                          String endDate) {
        log.info("{} ::getBitcoinPriceAndPopularityData invoked with parameters: startDate={}, endDate={}", API_TAG, startDate, endDate);
        long start = System.currentTimeMillis();

        Date startDateObj;
        Date endDateObj;

        try {
            startDateObj = DateUtils.formatter.parse(startDate);
            endDateObj = DateUtils.formatter.parse(endDate);
        } catch (Exception e) {
            log.warn("{} Exception while parsing dates", API_TAG, e);
            throw new BadRequestException("Invalid date(s)");
        }

        List<BitcoinPricePopularityDataModel> bitcoinPricePopularityDataModelList = tweetDataFacade
                .getBitcoinPriceAndPopularityData(startDateObj, endDateObj)
                .toList()
                .blockingGet();

        bitcoinPricePopularityDataModelList.sort(Comparator.comparing(BitcoinPricePopularityDataModel::getDate));

        log.info("{} ::getBitcoinPriceAndPopularityData finished in {} ms", API_TAG, (System.currentTimeMillis() - start));
        return bitcoinPricePopularityDataModelList;
    }

    @GET
    @Path("daily-volume")
    @AuthenticationRequired
    public List<TweetVolumeDataModel> getDailyVolume(@NotNull(message = "startDate cannot be null")
                                                     @Size(message = "startDate cannot be empty", min = 1)
                                                     @QueryParam("startDate")
                                                             String startDate,

                                                     @NotNull(message = "endDate cannot be null")
                                                     @Size(message = "endDate cannot be empty", min = 1)
                                                     @QueryParam("endDate")
                                                             String endDate) {

        log.info("{} ::getDailyVolume invoked with parameters: startDate={}, endDate={}", API_TAG, startDate, endDate);
        long start = System.currentTimeMillis();

        Date startDateObj;
        Date endDateObj;

        try {
            startDateObj = DateUtils.formatter.parse(startDate);
            endDateObj = DateUtils.formatter.parse(endDate);
        } catch (Exception e) {
            log.warn("{} Exception while parsing dates", API_TAG, e);
            throw new BadRequestException("Invalid date(s)");
        }

        List<TweetVolumeDataModel> tweetVolumeDataModelList = tweetDataFacade
                .getTweetVolumeData(startDateObj, endDateObj)
                .toList()
                .blockingGet();

        log.info("{} ::getDailyVolume finished in {} ms", API_TAG, (System.currentTimeMillis() - start));
        return tweetVolumeDataModelList;
    }

    @GET
    @Path("hourly-volume")
    @AuthenticationRequired
    public List<TweetVolumeHourlyDataModel> getHourlyVolume(@NotNull(message = "date cannot be null")
                                                            @Size(message = "date cannot be empty", min = 1)
                                                            @QueryParam("date")
                                                                    String date) {
        log.info("{} ::getHourlyVolume invoked with parameters: date={}", API_TAG, date);
        long start = System.currentTimeMillis();

        Date dateObj;

        try {
            dateObj = DateUtils.formatter.parse(date);
        } catch (Exception e) {
            log.warn("{} Exception while parsing date", API_TAG, e);
            throw new BadRequestException("Invalid date(s)");
        }

        List<TweetVolumeHourlyDataModel> tweetVolumeHourlyDataModelList = tweetDataFacade
                .getTweetVolumeHourlyDataModel(dateObj)
                .toList()
                .blockingGet();

        tweetVolumeHourlyDataModelList.sort((o1, o2) -> o1.getHour().compareToIgnoreCase(o2.getHour()));

        log.info("{} ::getHourlyVolume finished in {} ms", API_TAG, (System.currentTimeMillis() - start));
        return tweetVolumeHourlyDataModelList;
    }
}
