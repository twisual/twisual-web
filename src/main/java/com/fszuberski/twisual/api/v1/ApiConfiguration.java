package com.fszuberski.twisual.api.v1;

import com.fszuberski.twisual.api.v1.exception.mapper.*;
import com.fszuberski.twisual.api.v1.feature.FilterBindingFeature;
import com.fszuberski.twisual.api.v1.filter.cors.CorsFilter;
import com.fszuberski.twisual.api.v1.resource.NumericResource;
import com.fszuberski.twisual.api.v1.resource.TestResource;
import com.fszuberski.twisual.api.v1.resource.TweetResource;
import com.fszuberski.twisual.api.v1.resource.UserResource;
import lombok.extern.slf4j.Slf4j;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@Slf4j
@Component
@ApplicationPath("api/1")
public class ApiConfiguration extends ResourceConfig {

    @PostConstruct
    public void init() {
        // can't use classpath scanning
        // https://www.ivankrizsan.se/2016/12/06/jersey-and-spring-boot-standalone-jar-files/

        initializeResources();
        initializeFeatures();
        initializeFilters();
        initializeExceptionMappers();

        log.info("{} initialized", API_TAG);
    }

    protected void initializeCorsFilter(boolean corsDisabled) {
        if (corsDisabled) {
            CorsFilter corsFilter = new CorsFilter();
            corsFilter.getAllowedOrigins().add("*");
            corsFilter.setAllowedMethods("OPTIONS, GET, POST, DELETE, PUT, PATCH");
            register(corsFilter);

            log.warn("=======================================================================================================");
            log.warn("===== {} IMPORTANT: CORS is disabled! THIS CAN'T HAPPEN IN PRODUCTION - SECURITY ISSUE ", API_TAG);
            log.warn("=======================================================================================================");
        }
    }

    private void initializeResources() {
        register(NumericResource.class);
        register(TestResource.class);
        register(TweetResource.class);
        register(UserResource.class);
    }

    private void initializeFeatures() {
        register(FilterBindingFeature.class);
    }

    private void initializeFilters() {
        initializeCorsFilter(true);
    }

    private void initializeExceptionMappers() {
        register(BadRequestExceptionMapper.class);
        register(ConstraintViolationExceptionMapper.class);
        register(InternalServerErrorExceptionMapper.class);
        register(NotAuthorizedExceptionMapper.class);
        register(NotFoundExceptionMapper.class);
    }
}
