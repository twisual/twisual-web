package com.fszuberski.twisual.api.v1.exception.mapper;

import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@Slf4j
public class InternalServerErrorExceptionMapper implements ExceptionMapper<InternalServerErrorException>
{
    @Override
    public Response toResponse(InternalServerErrorException exception)
    {
        log.debug("{} {} has been invoked", API_TAG, this.getClass().getSimpleName());
        return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(exception.getMessage())
                .build();
    }
}
