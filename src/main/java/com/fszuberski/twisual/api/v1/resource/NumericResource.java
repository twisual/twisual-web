package com.fszuberski.twisual.api.v1.resource;

import com.fszuberski.twisual.business.boundary.GeneralStatisticsFacade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import static com.fszuberski.twisual.api.v1.Constants.API_TAG;

@Slf4j
@Component
@Path("numeric")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class NumericResource {

    private GeneralStatisticsFacade generalStatisticsFacade;

    @Autowired
    public void setGeneralStatisticsFacade(GeneralStatisticsFacade generalStatisticsFacade) {
        this.generalStatisticsFacade = generalStatisticsFacade;
    }

    @GET
    @Path("tweet-count")
    public Long tweetCount() {
        log.info("{} ::tweetCount invoked", API_TAG);
        return generalStatisticsFacade.tweetCount().blockingGet();
    }
}
