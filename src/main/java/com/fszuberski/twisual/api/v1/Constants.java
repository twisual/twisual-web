package com.fszuberski.twisual.api.v1;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.fszuberski.twisual.api.v1.filter.hmac.HmacHeaders.HEADER_X_TWISUAL_AUTHORIZATION;
import static com.fszuberski.twisual.api.v1.filter.hmac.HmacHeaders.HEADER_X_TWISUAL_SIGNATURE;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static final String API_TAG = "[API/v1]";

    // standard header content
    public static final String HEADER_WWW_AUTHENTICATE_WRONG_LOGIN_OR_PASSWORD = "Wrong login or password";
    public static final String HEADER_WWW_AUTHENTICATE_AUTHORIZATION_MISSING_OR_INVALID = HEADER_X_TWISUAL_AUTHORIZATION + " missing or invalid";
    public static final String HEADER_WWW_AUTHENTICATE_SIGNATURE_MISSING_OR_INVALID = HEADER_X_TWISUAL_SIGNATURE + " missing or invalid";
    public static final String HEADER_WWW_AUTHENTICATE_SESSION_EXPIRED = "Session expired";
}
