package com.fszuberski.twisual.api.v1.filter.cors;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.container.*;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
@PreMatching
public class CorsFilter implements ContainerRequestFilter, ContainerResponseFilter
{
    @Getter
    @Setter
    protected boolean allowCredentials = true;

    @Getter
    @Setter
    protected String allowedMethods;

    @Getter
    @Setter
    protected String allowedHeaders;

    @Getter
    protected Set<String> allowedOrigins = new HashSet<>();

    @Getter
    @Setter
    protected String exposedHeaders;

    @Getter
    @Setter
    protected int corsMaxAge = -1;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException
    {
        log.trace("::filter invoked with parameters: requestContext={}", requestContext);
        String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
        if (origin == null)
        {
            return;
        }
        if (requestContext.getMethod().equalsIgnoreCase("OPTIONS"))
        {
            preflight(origin, requestContext);
        } else
        {
            checkOrigin(requestContext, origin);
        }
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException
    {
        log.trace("::filter invoked with parameters: requestContext={}, responseContext={}", requestContext, responseContext);
        String origin = requestContext.getHeaderString(CorsHeaders.ORIGIN);
        if (origin == null || requestContext.getMethod().equalsIgnoreCase("OPTIONS") || requestContext.getProperty("cors.failure") != null)
        {
            // don't do anything if origin is null, its an OPTIONS request, or cors.failure is set
            return;
        }
        responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
        responseContext.getHeaders().putSingle(CorsHeaders.VARY, CorsHeaders.ORIGIN);
        if (allowCredentials)
        {
            responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
        }

        if (exposedHeaders != null)
        {
            responseContext.getHeaders().putSingle(CorsHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, exposedHeaders);
        }
    }


    protected void preflight(String origin, ContainerRequestContext requestContext) throws IOException
    {
        log.trace("::preflight invoked with parameters: origin={}, requestContext={}", origin, requestContext);
        checkOrigin(requestContext, origin);

        Response.ResponseBuilder builder = Response.ok();
        builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
        builder.header(CorsHeaders.VARY, CorsHeaders.ORIGIN);
        if (allowCredentials) builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
        String requestMethods = requestContext.getHeaderString(CorsHeaders.ACCESS_CONTROL_REQUEST_METHOD);
        if (requestMethods != null)
        {
            if (allowedMethods != null)
            {
                requestMethods = this.allowedMethods;
            }
            builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_METHODS, requestMethods);
        }
        String allowHeaders = requestContext.getHeaderString(CorsHeaders.ACCESS_CONTROL_REQUEST_HEADERS);
        if (allowHeaders != null)
        {
            if (allowedHeaders != null)
            {
                allowHeaders = this.allowedHeaders;
            }
            builder.header(CorsHeaders.ACCESS_CONTROL_ALLOW_HEADERS, allowHeaders);
        }
        if (corsMaxAge > -1)
        {
            builder.header(CorsHeaders.ACCESS_CONTROL_MAX_AGE, corsMaxAge);
        }
        requestContext.abortWith(builder.build());

    }

    protected void checkOrigin(ContainerRequestContext requestContext, String origin)
    {
        log.trace("::checkOrigin invoked with parameters: requestContext={}, origin={}", requestContext, origin);
        if (!allowedOrigins.contains("*") && !allowedOrigins.contains(origin))
        {
            requestContext.setProperty("cors.failure", true);
            throw new ForbiddenException("Origin not allowed: " + origin);
        }
    }
}