package com.fszuberski.twisual.business.boundary;

import com.fszuberski.twisual.common.enums.AccountStatus;
import com.fszuberski.twisual.common.manager.ThreadPoolManager;
import com.fszuberski.twisual.common.util.HashUtils;
import com.fszuberski.twisual.common.util.StringUtils;
import com.fszuberski.twisual.data.boundary.TwisualUserRepository;
import com.fszuberski.twisual.data.entity.TwisualUser;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TwisualUserFacade {

    private TwisualUserRepository twisualUserRepository;
    private ThreadPoolManager threadPoolManager;

    @Autowired
    public void setTwisualUserRepository(TwisualUserRepository twisualUserRepository) {
        this.twisualUserRepository = twisualUserRepository;
    }

    @Autowired
    public void setThreadPoolManager(ThreadPoolManager threadPoolManager) {
        this.threadPoolManager = threadPoolManager;
    }

    public Maybe<TwisualUser> findByLoginAndPassword(String login, String password) {
        return twisualUserRepository
                .findByLogin(login)
                .flatMap(user -> {
                    String hashedPassword = HashUtils.generatePasswordHash(password, user.getPasswordSalt());

                    if (hashedPassword.equals(user.getPasswordHash())) {
                        return Maybe.just(user);
                    }

                    return Maybe.empty();
                });
    }

    public void createDefaultAccounts() {

        String randomSalt = StringUtils.randomString(32);
        Single<TwisualUser> defaultUserSingle1 = Single.just(TwisualUser
                .builder()
                .registeredAt(new Date())
                .login("fszuberski")
                .email("filip.szuberski@gmail.com")
                .passwordHash(HashUtils.generatePasswordHash("123sikor", randomSalt))
                .passwordSalt(randomSalt)
                .sessionLength(999999)
                .accountStatus(AccountStatus.ACTIVE.getId())
                .build()
        );

        randomSalt = StringUtils.randomString(32);
        Single<TwisualUser> defaultUserSingle2 = Single.just(TwisualUser
                .builder()
                .registeredAt(new Date())
                .login("dkrol")
                .email("dariusz.krol@pwr.edu.pl")
                .passwordHash(HashUtils.generatePasswordHash("vizTwitter2018", randomSalt))
                .passwordSalt(randomSalt)
                .sessionLength(999999)
                .accountStatus(AccountStatus.ACTIVE.getId())
                .build()
        );

        twisualUserRepository
                .findByLogin("fszuberski")
                .switchIfEmpty(defaultUserSingle1)
                .flatMap(twisualUser -> twisualUserRepository
                        .save(twisualUser))
                .subscribeOn(Schedulers.from(threadPoolManager.getAsyncThreadPool()))
                .subscribe();

        twisualUserRepository
                .findByLogin("dkrol")
                .switchIfEmpty(defaultUserSingle2)
                .flatMap(twisualUser -> twisualUserRepository
                        .save(twisualUser))
                .subscribeOn(Schedulers.from(threadPoolManager.getAsyncThreadPool()))
                .subscribe();
    }
}
