package com.fszuberski.twisual.business.boundary;

import com.fszuberski.twisual.data.boundary.TwitterStatusRepository;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeneralStatisticsFacade {

    private TwitterStatusRepository twitterStatusRepository;

    @Autowired
    public void setTwitterStatusRepository(TwitterStatusRepository twitterStatusRepository) {
        this.twitterStatusRepository = twitterStatusRepository;
    }

    public Single<Long> tweetCount() {
        return twitterStatusRepository.count();
    }
}
