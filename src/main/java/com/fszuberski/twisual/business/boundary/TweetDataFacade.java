package com.fszuberski.twisual.business.boundary;

import com.fszuberski.twisual.business.control.BitcoinManager;
import com.fszuberski.twisual.business.control.TwitterStatisticsRanker;
import com.fszuberski.twisual.business.control.WordTreeDataConverter;
import com.fszuberski.twisual.business.control.merge.TwitterStatusStatisticsMerger;
import com.fszuberski.twisual.business.entity.*;
import com.fszuberski.twisual.common.manager.ThreadPoolManager;
import com.fszuberski.twisual.common.util.DateUtils;
import com.fszuberski.twisual.data.boundary.TwitterStatusStatisticsRepository;
import com.fszuberski.twisual.data.entity.GeoLocation;
import com.fszuberski.twisual.data.entity.TwitterStatusStatistics;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.fszuberski.twisual.common.util.DateUtils.dateOnlyFormat;
import static com.fszuberski.twisual.common.util.DateUtils.hourOnly;
import static com.fszuberski.twisual.common.util.DateUtils.isSameDay;

@Slf4j
@Service
public class TweetDataFacade {

    private TwitterStatusStatisticsRepository twitterStatusStatisticsRepository;
    private TwitterStatusStatisticsMerger twitterStatusStatisticsMerger;
    private TwitterStatisticsRanker twitterStatisticsRanker;
    private WordTreeDataConverter wordTreeDataConverter;
    private BitcoinManager bitcoinManager;
    private ThreadPoolManager threadPoolManager;

    @Autowired
    public void setTwitterStatusStatisticsRepository(TwitterStatusStatisticsRepository twitterStatusStatisticsRepository) {
        this.twitterStatusStatisticsRepository = twitterStatusStatisticsRepository;
    }

    @Autowired
    public void setTwitterStatusStatisticsMerger(TwitterStatusStatisticsMerger twitterStatusStatisticsMerger) {
        this.twitterStatusStatisticsMerger = twitterStatusStatisticsMerger;
    }

    @Autowired
    public void setTwitterStatisticsRanker(TwitterStatisticsRanker twitterStatisticsRanker) {
        this.twitterStatisticsRanker = twitterStatisticsRanker;
    }

    @Autowired
    public void setWordTreeDataConverter(WordTreeDataConverter wordTreeDataConverter) {
        this.wordTreeDataConverter = wordTreeDataConverter;
    }

    @Autowired
    public void setBitcoinManager(BitcoinManager bitcoinManager) {
        this.bitcoinManager = bitcoinManager;
    }

    @Autowired
    public void setThreadPoolManager(ThreadPoolManager threadPoolManager) {
        this.threadPoolManager = threadPoolManager;
    }

    public Observable<GeoLocation> tweetGeoLocationData(Date startDate, Date endDate) {

        try {
            return twitterStatusStatisticsRepository
                    .geoLocationData(startDate, endDate)
                    .flatMap(twitterStatusStatistics -> Observable.just(twitterStatusStatistics.getGeoLocations()))
                    .flatMap(Observable::fromIterable)
                    .subscribeOn(Schedulers.from(threadPoolManager.getAsyncThreadPool()));

        } catch (Exception e) {
            log.warn("Exception while retrieving tweet geo location data", e);
        }

        return Observable.empty();
    }


    public Single<WordTreeDataModel> getWordTreeData(Date startDate, Date endDate) {
        return twitterStatusStatisticsRepository
                .getWordTreeData(startDate, endDate)
                .reduce(new TwitterStatusStatistics(), (mergedStatistics, statistics) -> twitterStatusStatisticsMerger
                        .merge(mergedStatistics, statistics))
                .map(twitterStatusStatistics -> twitterStatisticsRanker.rankAndLimitWordsForResponse(twitterStatusStatistics))
                .map(twitterStatusStatistics -> wordTreeDataConverter.convert(twitterStatusStatistics))
                .subscribeOn(Schedulers.from(threadPoolManager.getAsyncThreadPool()));
    }

    public Observable<BitcoinPricePopularityDataModel> getBitcoinPriceAndPopularityData(Date startDate, Date endDate) {
        return bitcoinManager
                .getBitcoinDataBetweenDates(startDate, endDate)
                .map(bitcoinPriceData -> {
                    if (bitcoinPriceData.getPopularity() == null) {
                        Integer popularity = bitcoinManager.getBitcoinPopularityForDay(bitcoinPriceData.getTime()).blockingFirst();
                        bitcoinPriceData.setPopularity(popularity);

                        // save to db
                        bitcoinManager.updateData(bitcoinPriceData).blockingGet();
                    }

                    return new BitcoinPricePopularityDataModel(
                            dateOnlyFormat().format(bitcoinPriceData.getTime()),
                            bitcoinPriceData.getAverage(),
                            bitcoinPriceData.getPopularity()
                    );
                })
                .subscribeOn(Schedulers.from(threadPoolManager.getAsyncThreadPool()));
    }

    public Observable<TweetVolumeDataModel> getTweetVolumeData(Date startDate, Date endDate) {
        return Observable
                .fromIterable(DateUtils.generateDatePairsBetween(startDate, endDate))
                .flatMap(datePair -> twitterStatusStatisticsRepository
                        .tweetVolumeData(datePair.getFirst(), datePair.getSecond())
                        .reduce(new TwitterStatusStatistics(datePair.getFirst(), datePair.getSecond()), (mergedStatistics, statistics) -> twitterStatusStatisticsMerger
                                .merge(mergedStatistics, statistics))
                        .toObservable())
                .map(twitterStatusStatistics -> new TweetVolumeDataModel(
                        dateOnlyFormat().format(twitterStatusStatistics.getCreatedAtStartDate()), twitterStatusStatistics.getTweetCount()))
                .subscribeOn(Schedulers.from(threadPoolManager.getAsyncThreadPool()));
    }

    public Observable<TweetVolumeHourlyDataModel> getTweetVolumeHourlyDataModel(Date date) {
        return Observable
                .fromIterable(DateUtils.generateHourlyDatePairsForDateWithFirstDateOfTomorrow(date))
                .flatMap(datePair -> twitterStatusStatisticsRepository
                        .tweetVolumeData(datePair.getFirst(), datePair.getSecond())
                        .reduce(new TwitterStatusStatistics(datePair.getFirst(), datePair.getSecond()), (mergedStatistics, statistics) -> twitterStatusStatisticsMerger
                                .merge(mergedStatistics, statistics)).toObservable())
                .map(twitterStatusStatistics -> new TweetVolumeHourlyDataModel(
                        isSameDay(twitterStatusStatistics.getCreatedAtStartDate(), date)
                                ? hourOnly().format(twitterStatusStatistics.getCreatedAtStartDate())
                                : "24",
                        twitterStatusStatistics.getTweetCount()))
                .subscribeOn(Schedulers.from(threadPoolManager.getAsyncThreadPool()));
    }
}
