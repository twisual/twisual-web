package com.fszuberski.twisual.business.control;

import com.fszuberski.twisual.business.control.merge.TwitterStatusStatisticsMerger;
import com.fszuberski.twisual.common.util.DateUtils;
import com.fszuberski.twisual.data.boundary.BitcoinPriceDataRepository;
import com.fszuberski.twisual.data.boundary.TwitterStatusStatisticsRepository;
import com.fszuberski.twisual.data.entity.BitcoinPriceData;
import com.fszuberski.twisual.data.entity.TwitterStatusStatistics;
import io.reactivex.Observable;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import static com.fszuberski.twisual.common.util.DateUtils.dayAfterFirstMoment;
import static com.fszuberski.twisual.common.util.DateUtils.dayBeforeFirstMoment;
import static com.fszuberski.twisual.common.util.DateUtils.longFormat;

@Slf4j
@Component
public class BitcoinManager {

    private BitcoinPriceDataRepository bitcoinPriceDataRepository;
    private TwitterStatusStatisticsRepository twitterStatusStatisticsRepository;
    private TwitterStatusStatisticsMerger twitterStatusStatisticsMerger;

    @Autowired
    public void setBitcoinPriceDataRepository(BitcoinPriceDataRepository bitcoinPriceDataRepository) {
        this.bitcoinPriceDataRepository = bitcoinPriceDataRepository;
    }

    @Autowired
    public void setTwitterStatusStatisticsRepository(TwitterStatusStatisticsRepository twitterStatusStatisticsRepository) {
        this.twitterStatusStatisticsRepository = twitterStatusStatisticsRepository;
    }

    @Autowired
    public void setTwitterStatusStatisticsMerger(TwitterStatusStatisticsMerger twitterStatusStatisticsMerger) {
        this.twitterStatusStatisticsMerger = twitterStatusStatisticsMerger;
    }

    public Observable<BitcoinPriceData> getBitcoinDataBetweenDates(Date startDate, Date endDate) {
        return bitcoinPriceDataRepository
                .findBetweenDates(dayAfterFirstMoment(startDate), dayAfterFirstMoment(endDate))
                .map(bitcoinPriceData -> {
                    bitcoinPriceData
                            .setTime(dayBeforeFirstMoment(bitcoinPriceData.getTime()));

                    return bitcoinPriceData;
                })
                .doOnNext(bitcoinPriceData -> log.info("bitcoin data: {}", bitcoinPriceData))
                .doOnError(e -> log.warn("Exception while retrieving BTC price between dates: startDate={}, endDate={}", startDate, endDate, e));
    }

    public Observable<Integer> getBitcoinPopularityForDay(Date date) {
        return Observable
                .just(Pair.of(DateUtils.firstMomentForDate(date), DateUtils.dayAfterFirstMoment(date)))
                .flatMap(datePair -> twitterStatusStatisticsRepository
                        .bitcoinPopularityData(datePair.getFirst(), datePair.getSecond())
                        .reduce(new TwitterStatusStatistics(), (mergedStatistics, statistics) -> twitterStatusStatisticsMerger
                                .merge(mergedStatistics, statistics))
                        .toObservable(), 1)
                .map(twitterStatusStatistics -> {
                    int count = 0;
                    for (String hashtag : twitterStatusStatistics.getHashtagEntities().keySet()) {
                        if (hashtag.equalsIgnoreCase("btc") || hashtag.equalsIgnoreCase("bitcoin")) {
                            count += twitterStatusStatistics.getHashtagEntities().get(hashtag);
                        }
                    }

                    return count;
                });
    }

    public Single<BitcoinPriceData> updateData(BitcoinPriceData bitcoinPriceData) {
        return bitcoinPriceDataRepository
                .save(bitcoinPriceData);
    }
}
