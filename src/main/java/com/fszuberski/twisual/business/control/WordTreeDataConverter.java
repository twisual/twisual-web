package com.fszuberski.twisual.business.control;

import com.fszuberski.twisual.business.entity.WordTreeDataModel;
import com.fszuberski.twisual.data.entity.TwitterStatusStatistics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.*;

@Slf4j
@Component
public class WordTreeDataConverter {

    private static final int MAX_PER_GROUP = 10;
    private static final int MAX_WORDS_PER_LANGUAGE = 10;

    public WordTreeDataModel convert(TwitterStatusStatistics twitterStatusStatistics) {
        WordTreeDataModel root = new WordTreeDataModel();
        root.setChildren(new ArrayList<>());
        root.setName("languages and words");

        for (Map.Entry<String, HashMap<String, Integer>> languageEntry : twitterStatusStatistics.getWords().entrySet()) {
            int languageCount = 0;

            WordTreeDataModel languageNode = new WordTreeDataModel();
            languageNode.setChildren(new ArrayList<>());

            for (Map.Entry<String, Integer> wordEntry : languageEntry.getValue().entrySet()) {

                WordTreeDataModel wordNode = new WordTreeDataModel();
                wordNode.setName(wordEntry.getKey());
                wordNode.setAttributes(new WordTreeDataModel.Attributes(wordEntry.getValue()));

                languageCount += wordEntry.getValue();
                languageNode.getChildren().add(wordNode);
            }

            languageNode.setName(languageEntry.getKey());
            languageNode.setAttributes(new WordTreeDataModel.Attributes(languageCount));
            root.getChildren().add(languageNode);
        }

        root = rankWords(root);
        root = divideLanguages(root);

        return root;
    }

    public WordTreeDataModel divideLanguages(WordTreeDataModel wordTreeDataModel) {

        List<WordTreeDataModel> languageGroups = new ArrayList<>();
        List<WordTreeDataModel> previousRootChildren = wordTreeDataModel.getChildren();
        wordTreeDataModel.setChildren(new ArrayList<>());

        double numberOfLanguageGroups = ((double) previousRootChildren.size()) / MAX_PER_GROUP;
        int count = 0;

        while (count <= numberOfLanguageGroups) {
            WordTreeDataModel languageGroupNode = new WordTreeDataModel();
            languageGroupNode.setName("group " + (count + 1));
            languageGroupNode.setChildren(new ArrayList<>());
            ++count;

            languageGroups.add(languageGroupNode);
        }

        List<WordTreeDataModel> newRootChildren = rankLanguages(previousRootChildren);

        // concurrent modification
        for (WordTreeDataModel languageGroup : languageGroups) {
            while (languageGroup.getChildren().size() <= MAX_PER_GROUP && newRootChildren.size() > 0) {
                languageGroup.getChildren().add(newRootChildren.get(0));
                newRootChildren.remove(0);
            }
        }

        wordTreeDataModel.setChildren(languageGroups);

        return wordTreeDataModel;
    }

    public List<WordTreeDataModel> rankLanguages(List<WordTreeDataModel> wordTreeDataModelList) {
        wordTreeDataModelList.sort((o1, o2) -> o2.getAttributes().getCount() - o1.getAttributes().getCount());
        return wordTreeDataModelList;
    }

    public WordTreeDataModel rankWords(WordTreeDataModel wordTreeDataModel) {

        for (WordTreeDataModel language : wordTreeDataModel.getChildren()) {
            List<WordTreeDataModel> words = language.getChildren();
            words.sort((o1, o2) -> o2.getAttributes().getCount() - o1.getAttributes().getCount());

            if (words.size() > MAX_WORDS_PER_LANGUAGE) {
                words = words.subList(0, MAX_WORDS_PER_LANGUAGE);
            }

            language.setChildren(words);
        }

        return wordTreeDataModel;
    }

}
