package com.fszuberski.twisual.business.control;

import com.fszuberski.twisual.data.entity.TwitterStatusStatistics;
import com.fszuberski.twisual.data.entity.TwitterUserStatistics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fszuberski.twisual.business.entity.language.Language.ENGLISH;
import static com.fszuberski.twisual.business.entity.language.Language.UNDEFINED;


@Slf4j
@Component
public class TwitterStatisticsRanker {

    public TwitterStatusStatistics rankAndLimitWordsForResponse(TwitterStatusStatistics twitterStatusStatistics) {
        twitterStatusStatistics
                .getWords()
                .forEach((outerKey, outerValue) -> {
                    if (outerKey.toLowerCase().equals(ENGLISH.getLanguageCode())
                            || outerKey.toLowerCase().equals(UNDEFINED.getLanguageCode())) {
                        outerValue
                                .entrySet()
                                .removeIf(entry -> entry.getValue() < 2);
                    }
                });

        for (Map.Entry<String, HashMap<String, Integer>> singleLanguageEntry : twitterStatusStatistics.getWords().entrySet()) {
            singleLanguageEntry.setValue(rank(singleLanguageEntry.getValue(), 10));
        }

        return twitterStatusStatistics;
    }

    public void rank(TwitterStatusStatistics twitterStatusStatistics) {
        rank(twitterStatusStatistics, false);
    }

    public void rank(TwitterStatusStatistics twitterStatusStatistics, boolean withLimits) {
        // extra optimization for wordMap
        twitterStatusStatistics
                .getWords()
                .forEach((outerKey, outerValue) -> {
                    if (outerKey.toLowerCase().equals(ENGLISH.getLanguageCode())
                            || outerKey.toLowerCase().equals(UNDEFINED.getLanguageCode())) {
                        outerValue
                                .entrySet()
                                .removeIf(entry -> entry.getValue() < 1);
                    }
                });

        rank(twitterStatusStatistics.getSources());
        rank(twitterStatusStatistics.getLanguages());
        rank(twitterStatusStatistics.getUserMentionEntities());
        rank(twitterStatusStatistics.getUrlEntities());
        rank(twitterStatusStatistics.getHashtagEntities());
        rank(twitterStatusStatistics.getMediaEntityTypes());
        rank(twitterStatusStatistics.getSymbolEntityTexts());
        rank(twitterStatusStatistics.getPlaceTypes());
    }

    public void rank(TwitterUserStatistics twitterUserStatistics) {
        rank(twitterUserStatistics, false);
    }

    public void rank(TwitterUserStatistics twitterUserStatistics, boolean withLimits) {
        // extra optimization for wordMap
        twitterUserStatistics
                .getDescriptionWords()
                .forEach((outerKey, outerValue) -> {
                    if (outerKey.toLowerCase().equals(ENGLISH.getLanguageCode())
                            || outerKey.toLowerCase().equals(UNDEFINED.getLanguageCode())) {
                        outerValue
                                .entrySet()
                                .removeIf(entry -> entry.getValue() < 1);
                    }
                });


        rank(twitterUserStatistics.getLocations());
        rank(twitterUserStatistics.getLanguages());
        rank(twitterUserStatistics.getProfileBackgroundColors());
        rank(twitterUserStatistics.getProfileTextColors());
        rank(twitterUserStatistics.getProfileLinkColors());
        rank(twitterUserStatistics.getProfileSidebarFillColors());
        rank(twitterUserStatistics.getProfileSidebarBorderColors());
        rank(twitterUserStatistics.getLocations());
    }

    protected void rank(Map<String, Integer> map) {
        rank((HashMap<String, Integer>) map, 0);
    }

    protected HashMap<String, Integer> rank(HashMap<String, Integer> map, int numberOfValues) {
        List<Map.Entry<String, Integer>> sortedList = toSortedList(map);

        if (map.size() > numberOfValues && numberOfValues != 0) {
            sortedList = sortedList.subList(0, numberOfValues);
        }

        map.clear();

        for (Map.Entry entry : sortedList) {
            map.put((String) entry.getKey(), (Integer) entry.getValue());
        }

        return map;
    }

    protected List<Map.Entry<String, Integer>> toSortedList(Map<String, Integer> map) {
        List<Map.Entry<String, Integer>> sortedList = new ArrayList<>(map.entrySet());
        sortedList.sort((o1, o2) -> o2.getValue() - o1.getValue());

        return sortedList;
    }
}
