package com.fszuberski.twisual.business.control.merge;

import com.fszuberski.twisual.common.enums.TimeType;
import com.fszuberski.twisual.data.entity.TwitterStatusStatistics;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.fszuberski.twisual.common.util.DateUtils.dayOfWeek;

@Component
public class TwitterStatusStatisticsMerger extends StatisticsMerger<TwitterStatusStatistics> {



    @Override
    @SuppressWarnings("Duplicates")
    public TwitterStatusStatistics mark(TwitterStatusStatistics statistics, Date startTime, Date endTime, TimeType timeType) {
        twitterStatisticsRanker.rank(statistics);
        statistics.setCreatedAtStartDate(startTime);
        statistics.setCreatedAtEndDate(endTime);
        statistics.setDayOfWeek(dayOfWeek(startTime));
        statistics.setTimeType(timeType.getId());

        return statistics;
    }

    @Override
    public TwitterStatusStatistics merge(TwitterStatusStatistics accumulator, TwitterStatusStatistics statistics) {
        accumulator.setTweetCount(accumulator.getTweetCount() + statistics.getTweetCount());
        accumulator.setTruncatedCount(accumulator.getTruncatedCount() + statistics.getTruncatedCount());
        accumulator.setFavoritedCount(accumulator.getFavoritedCount() + statistics.getFavoritedCount());
        accumulator.setRetweetedCount(accumulator.getRetweetedCount() + statistics.getRetweetedCount());
        accumulator.setPossiblySensitiveCount(accumulator.getPossiblySensitiveCount() + statistics.getPossiblySensitiveCount());
        accumulator.setUserMentionEntityCount(accumulator.getUserMentionEntityCount() + statistics.getUserMentionEntityCount());
        accumulator.setUrlEntityCount(accumulator.getUrlEntityCount() + statistics.getUrlEntityCount());
        accumulator.setHashtagEntityCount(accumulator.getHashtagEntityCount() + statistics.getHashtagEntityCount());
        accumulator.setMediaEntityCount(accumulator.getMediaEntityCount() + statistics.getMediaEntityCount());
        accumulator.setSymbolEntityCount(accumulator.getSymbolEntityCount() + statistics.getSymbolEntityCount());

        accumulator.getGeoLocations().addAll(statistics.getGeoLocations());
        accumulator.getPlaces().addAll(statistics.getPlaces());

        accumulator.setSources(mergeMaps(accumulator.getSources(), statistics.getSources()));
        accumulator.setLanguages(mergeMaps(accumulator.getLanguages(), statistics.getLanguages()));
        accumulator.setUserMentionEntities(mergeMaps(accumulator.getUserMentionEntities(), statistics.getUserMentionEntities()));
        accumulator.setUrlEntities(mergeMaps(accumulator.getUrlEntities(), statistics.getUrlEntities()));
        accumulator.setHashtagEntities(mergeMaps(accumulator.getHashtagEntities(), statistics.getHashtagEntities()));
        accumulator.setMediaEntityTypes(mergeMaps(accumulator.getMediaEntityTypes(), statistics.getMediaEntityTypes()));
        accumulator.setMediaEntityUrls(mergeMaps(accumulator.getMediaEntityUrls(), statistics.getMediaEntityUrls()));
        accumulator.setSymbolEntityTexts(mergeMaps(accumulator.getSymbolEntityTexts(), statistics.getSymbolEntityTexts()));
        accumulator.setPlaceTypes(mergeMaps(accumulator.getPlaceTypes(), statistics.getPlaceTypes()));

        accumulator.setWords(mergeWordMaps(accumulator.getWords(), statistics.getWords()));

        return accumulator;
    }
}
