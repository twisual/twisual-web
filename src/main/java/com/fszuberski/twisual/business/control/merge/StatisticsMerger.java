package com.fszuberski.twisual.business.control.merge;

import com.fszuberski.twisual.business.control.TwitterStatisticsRanker;
import com.fszuberski.twisual.common.enums.TimeType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class StatisticsMerger<T> {

    TwitterStatisticsRanker twitterStatisticsRanker;

    @Autowired
    public void setTwitterStatisticsRanker(TwitterStatisticsRanker twitterStatisticsRanker) {
        this.twitterStatisticsRanker = twitterStatisticsRanker;
    }

    public abstract T mark(T statistics, Date startTime, Date endTime, TimeType timeType);

    public abstract T merge(T accumulator, T statistics);

    <S> Map<S, Integer> mergeMaps(Map<S, Integer> accumulatorMap, Map<S, Integer> statisticsMap) {
        for (Map.Entry<S, Integer> statisticsMapEntry : statisticsMap.entrySet()) {

            if (accumulatorMap.containsKey(statisticsMapEntry.getKey())) {
                accumulatorMap.put(statisticsMapEntry.getKey(), accumulatorMap.get(statisticsMapEntry.getKey()) + statisticsMapEntry.getValue());
            } else {
                accumulatorMap.put(statisticsMapEntry.getKey(), statisticsMapEntry.getValue());
            }
        }

        return accumulatorMap;
    }

    Map<String, HashMap<String, Integer>> mergeWordMaps(Map<String, HashMap<String, Integer>> accumulatorMap, Map<String, HashMap<String, Integer>> statisticsMap) {
        for (Map.Entry<String, HashMap<String, Integer>> statisticsLanguageEntry : statisticsMap.entrySet()) {

            if (accumulatorMap.containsKey(statisticsLanguageEntry.getKey())) {

                for (Map.Entry<String, Integer> statisticsWordEntry : statisticsLanguageEntry.getValue().entrySet()) {

                    if (accumulatorMap.get(statisticsLanguageEntry.getKey()).containsKey(statisticsWordEntry.getKey())) {

                        accumulatorMap.get(statisticsLanguageEntry.getKey()).put(
                                statisticsWordEntry.getKey(),
                                accumulatorMap.get(statisticsLanguageEntry.getKey()).get(statisticsWordEntry.getKey()) + statisticsWordEntry.getValue());

                    } else {
                        accumulatorMap.get(statisticsLanguageEntry.getKey()).put(statisticsWordEntry.getKey(), statisticsWordEntry.getValue());
                    }
                }

            } else {
                accumulatorMap.put(statisticsLanguageEntry.getKey(), statisticsLanguageEntry.getValue());
            }
        }

        return accumulatorMap;
    }
}
