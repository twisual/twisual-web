package com.fszuberski.twisual.business.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BitcoinPricePopularityDataModel {

    private String date;
    private Double price;
    private Integer popularity;
}
