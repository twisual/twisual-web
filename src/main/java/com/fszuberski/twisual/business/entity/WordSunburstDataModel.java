package com.fszuberski.twisual.business.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WordSunburstDataModel {

    private List<Child> children;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Child {
        private String name;
        private String hex;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private List<Child> children;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        private Integer value;
    }
}
