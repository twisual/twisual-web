package com.fszuberski.twisual.business.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TweetVolumeHourlyDataModel {

    private String hour;
    private Integer count;
}
