package com.fszuberski.twisual.business.entity.language;

import lombok.Getter;

public enum Language {

    DANISH("da"),
    GERMAN("de"),
    ENGLISH("en"),
    SPANISH("es"),
    ITALIAN("it"),
    DUTCH("nl"),
    PORTUGUESE("pt"),
    UNDEFINED("und");

    @Getter
    private String languageCode;

    Language(String languageCode) {
        this.languageCode = languageCode;
    }

    public static Language findByLanguageCode(String languageCode) {
        for (Language definedLanguage : values()) {
            if (definedLanguage.languageCode.equals(languageCode)) {
                return definedLanguage;
            }
        }

        return UNDEFINED;
    }
}