package com.fszuberski.twisual.business.entity.language;

import java.util.ArrayList;
import java.util.List;

public enum PartsOfSpeech {
    n("n", "noun"),
    v("v", "verb"),
    a("a", "adjective"),
    j("j", "adjective"),
    fw("fw", "foreign word"),
    x("x", "unknown"),
    backTicks("``", "unknown"),
    und("und", "undefined");

    private String asString;
    private String humanReadable;

    PartsOfSpeech(String asString, String humanReadable) {
        this.asString = asString;
        this.humanReadable = humanReadable;
    }

    public static List<String> asStringedList() {
        List<String> stringedList = new ArrayList<>();
        for (PartsOfSpeech singleValue : values()) {
            stringedList.add(singleValue.asString);
        }

        return stringedList;
    }
}
