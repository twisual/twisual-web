package com.fszuberski.twisual.business.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WordTreeDataModel {

    private String name;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Attributes attributes;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<WordTreeDataModel> children;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Attributes {
        private Integer count;
    }
}
