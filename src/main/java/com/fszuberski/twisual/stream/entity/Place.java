package com.fszuberski.twisual.stream.entity;

import com.fszuberski.twisual.data.entity.GeoLocation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Place {
    private String fullName;
    private String name;
    private String countryCode;
    private String placeType;
    private List<GeoLocation> geoLocationBoundaries;

    public Place(twitter4j.Place twitterPlace) {
        this.fullName = twitterPlace.getFullName();
        this.name = twitterPlace.getName();
        this.countryCode = twitterPlace.getCountryCode();
        this.placeType = twitterPlace.getPlaceType();

        this.geoLocationBoundaries = new ArrayList<>();
        for (twitter4j.GeoLocation[] twitterOuterGeoLocation : twitterPlace.getBoundingBoxCoordinates()) {
            for (twitter4j.GeoLocation twitterInnerGeoLocation : twitterOuterGeoLocation) {
                geoLocationBoundaries.add(new GeoLocation(twitterInnerGeoLocation));
            }
        }
    }

}
