package com.fszuberski.twisual.common;

import com.fszuberski.twisual.business.boundary.TwisualUserFacade;
import com.fszuberski.twisual.common.manager.ThreadPoolManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ApplicationContextListener {

    private ThreadPoolManager threadPoolManager;
    private TwisualUserFacade twisualUserFacade;

    @Autowired
    public void setThreadPoolManager(ThreadPoolManager threadPoolManager) {
        this.threadPoolManager = threadPoolManager;
    }

    @Autowired
    public void setTwisualUserFacade(TwisualUserFacade twisualUserFacade) {
        this.twisualUserFacade = twisualUserFacade;
    }

    @EventListener
    void contextRefreshed(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("ContextRefreshed: {}", contextRefreshedEvent);

        threadPoolManager.createAsyncThreadPool();
        log.info("Async thread pool initialized");

        twisualUserFacade.createDefaultAccounts();
    }

    @EventListener
    void contextClosed(ContextClosedEvent contextClosedEvent) {
        log.info("ContextRefreshed: {}", contextClosedEvent);

        threadPoolManager.shutdownAsynchronousExecutorPool();
        log.info("Async thread pool deinitialized");
    }
}
