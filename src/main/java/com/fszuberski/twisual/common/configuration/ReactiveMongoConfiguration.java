package com.fszuberski.twisual.common.configuration;

import com.fszuberski.twisual.common.manager.PropertiesManager;
import com.fszuberski.twisual.data.control.LinkedHashMapToGeoLocationConverter;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Configuration
public class ReactiveMongoConfiguration extends AbstractReactiveMongoConfiguration {

    private PropertiesManager propertiesManager;

    @Autowired
    public void setPropertiesManager(PropertiesManager propertiesManager) {
        this.propertiesManager = propertiesManager;
    }

    @Bean(name = "twisual")
    @Override
    public MongoClient reactiveMongoClient() {
        return MongoClients.create("mongodb://" + propertiesManager.getDbHost() + ":" + propertiesManager.getDbPort());
    }

    @Override
    protected String getDatabaseName() {
        return propertiesManager.getDbDatabase();
    }

    @Bean
    @Override
    public MongoCustomConversions customConversions() {
        List<Converter<?, ?>> converterList = new ArrayList<>();
        converterList.add(new LinkedHashMapToGeoLocationConverter());
        return new MongoCustomConversions(converterList);
    }

    @Bean
    @Override
    public MappingMongoConverter mappingMongoConverter() throws Exception {
        MappingMongoConverter converter = super.mappingMongoConverter();
        converter.setMapKeyDotReplacement(propertiesManager.getDbDotReplacement());

        return converter;
    }
}
