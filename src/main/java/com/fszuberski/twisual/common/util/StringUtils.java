package com.fszuberski.twisual.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.RandomStringGenerator;
import org.glassfish.jersey.message.internal.ReaderWriter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.container.ContainerRequestContext;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.Collator;
import java.text.Normalizer;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;
import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
public class StringUtils
{
    private static final RandomStringGenerator RANDOM_STRING_GENERATOR = new RandomStringGenerator
            .Builder()
            .withinRange('0', 'z')
            .filteredBy(LETTERS, DIGITS)
            .build();

    public static String randomString(int length)
    {
        return RANDOM_STRING_GENERATOR.generate(length);
    }

    // application.properties are encoded as ISO-8859-1
    public static String fromPropertiesAsUTF8(String text)
    {
        byte[] latin1 = text.getBytes(ISO_8859_1);
        return new String(latin1, UTF_8);
    }

    public static String readClassPathResourceContents(ClassPathResource classPathResource, Charset charset) {

        String data = "";
        try {
            byte[] byteData = FileCopyUtils.copyToByteArray(classPathResource.getInputStream());
            data = new String(byteData, charset);
        } catch (IOException e)
        {
            log.warn("Exception while leading class path resource contents", e);
        }

        return data;
    }

    public static String readRequestBody(ContainerRequestContext requestContext)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = requestContext.getEntityStream();

        final StringBuilder stringBuilder = new StringBuilder();
        try
        {
            if (inputStream.available() > 0)
            {
                ReaderWriter.writeTo(inputStream, outputStream);

                byte[] requestEntity = outputStream.toByteArray();
                requestContext.setEntityStream(new ByteArrayInputStream(requestEntity));

                return stringBuilder.append(new String(requestEntity)).toString();
            } else
            {
                return "";
            }
        } catch (Exception e)
        {
            log.warn("Exception while reading body from request.", e);
            throw new InternalServerErrorException("Cannot read request body.");
        }
    }
}
