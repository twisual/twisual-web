package com.fszuberski.twisual.common.util;

import org.springframework.data.util.Pair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.util.Calendar.DAY_OF_WEEK;

public class DateUtils {

    public static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    //------------------------------------------------------------------------------------------------------------------ formatting

    public static String timeOnlyWithoutSeconds(Date date)
    {
        return timeOnlyWithoutSecondsFormat().format(date);
    }

    //------------------------------------------------------------------------------------------------------------------ simple date format

    public static SimpleDateFormat timeOnlyWithoutSecondsFormat()
    {
        return new SimpleDateFormat("HH:mm");
    }

    public static SimpleDateFormat hourOnly() {
        return new SimpleDateFormat("HH");
    }

    public static SimpleDateFormat dateOnlyFormat()
    {
        return new SimpleDateFormat("yyyy-MM-dd");
    }

    public static SimpleDateFormat longFormat()
    {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    public static SimpleDateFormat fileSuffixFormat()
    {
        return new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS");
    }

    //

    public static boolean isSameDay(Date date1, Date date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(date1);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(date2);

        if (calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
                && calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH)
                && calendar1.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH)) {
            return true;
        }

        return false;
    }

    public static int dayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(DAY_OF_WEEK);
    }

    public static Date firstMomentForDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static String dayAfterFirstMoment(String dateStr) throws ParseException {
        Date date = longFormat().parse(dateStr);
        Date dayAfter = dayAfterFirstMoment(date);

        return longFormat().format(dayAfter);
    }

    public static Date dayAfterFirstMoment(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static String dayBeforeFirstMoment(String dateStr) throws ParseException {
        Date date = longFormat().parse(dateStr);
        Date dayBefore = dayBeforeFirstMoment(date);

        return longFormat().format(dayBefore);
    }

    public static Date dayBeforeFirstMoment(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static List<Date> generateDatesBetween(Date startDate, Date endDate) {
        List<Date> dateList = new ArrayList<>();


        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        while (calendar.getTime().before(endDate) || calendar.getTime().getTime() == endDate.getTime()) {
            dateList.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        return dateList;
    }

    public static List<Pair<Date, Date>> generateDatePairsBetween(Date startDate, Date endDate) {
        List<Date> dateList = generateDatesBetween(startDate, endDate);

        List<Pair<Date, Date>> datePairList = new ArrayList<>();
        for (Date singleDate : dateList) {
            datePairList.add(Pair.of(singleDate, dayAfterFirstMoment(singleDate)));
        }

        return datePairList;
    }

    public static List<Date> generateHourlyDatesForDate(Date date) {
        List<Date> dateList = new ArrayList<>();

        date = firstMomentForDate(date);
        Date tomorrow = dayAfterFirstMoment(date);

        Calendar calendar = Calendar.getInstance();
        while (date.before(tomorrow)) {
            dateList.add(date);

            calendar.setTime(date);
            calendar.add(Calendar.HOUR_OF_DAY, 1);

            date = calendar.getTime();
        }

        return dateList;
    }

    public static List<Pair<Date, Date>> generateHourlyDatePairsForDate(Date date) {
        List<Date> dateList = generateHourlyDatesForDate(date);

        List<Pair<Date, Date>> datePairList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        for (Date singleDate : dateList) {
            calendar.setTime(singleDate);
            calendar.add(Calendar.HOUR_OF_DAY, 1);

            datePairList.add(Pair.of(singleDate, calendar.getTime()));
        }
        return datePairList;

    }

    public static List<Date> generateHourlyDatesForDateWithFirstDateOfTomorrow(Date date) {
        List<Date> dateList = new ArrayList<>();

        date = firstMomentForDate(date);
        Date tomorrow = dayAfterFirstMoment(date);

        Calendar calendar = Calendar.getInstance();
        while (date.before(tomorrow) || date.getTime() == tomorrow.getTime()) {
            dateList.add(date);

            calendar.setTime(date);
            calendar.add(Calendar.HOUR_OF_DAY, 1);

            date = calendar.getTime();
        }

        return dateList;
    }

    public static List<Pair<Date, Date>> generateHourlyDatePairsForDateWithFirstDateOfTomorrow(Date date) {
        List<Date> dateList = generateHourlyDatesForDateWithFirstDateOfTomorrow(date);

        List<Pair<Date, Date>> datePairList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        for (Date singleDate : dateList) {
            calendar.setTime(singleDate);
            calendar.add(Calendar.HOUR_OF_DAY, 1);

            datePairList.add(Pair.of(singleDate, calendar.getTime()));
        }
        return datePairList;

    }
}
