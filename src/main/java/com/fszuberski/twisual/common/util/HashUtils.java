package com.fszuberski.twisual.common.util;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.fszuberski.twisual.common.util.StringUtils.randomString;
import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;
import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;

public class HashUtils
{
    public static String sha1(List<String> inputList)
    {
        StringBuilder sb = new StringBuilder();
        for (String arg : inputList)
        {
            sb.append(arg);
        }

        return sha1(sb.toString());
    }

    public static String sha1(String input)
    {
        return sha1Hex(input);
    }

    public static String sha256(List<String> inputList)
    {
        StringBuilder sb = new StringBuilder();
        for (String arg : inputList)
        {
            sb.append(arg);
        }

        return sha256(sb.toString());
    }

    public static String sha256(String input)
    {
        return sha256Hex(input);
    }

    public static String randomSha256()
    {
        return sha256(randomString(128) + UUID.randomUUID());
    }

    public static String randomHash() {
        return randomSha256();
    }

    public static String generatePasswordHash(String password, String salt)
    {
        return HashUtils.sha256(Arrays.asList(password, salt));
    }
}
