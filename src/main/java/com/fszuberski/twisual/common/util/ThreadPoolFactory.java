package com.fszuberski.twisual.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ThreadPoolFactory {

    public ThreadPoolTaskExecutor createThreadPoolTaskExecutor(String namePrefix, int corePoolSize, int maxPoolSize, int queueCapacity, int keepAliveSeconds,
                                                               int awaitTerminationSeconds, boolean waitForTasksToCompleteOnShutdown, boolean daemon) {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setThreadNamePrefix(namePrefix);

        threadPoolTaskExecutor.setCorePoolSize(corePoolSize);
        threadPoolTaskExecutor.setMaxPoolSize(maxPoolSize);

        threadPoolTaskExecutor.setQueueCapacity(queueCapacity);
        threadPoolTaskExecutor.setKeepAliveSeconds(keepAliveSeconds);
        threadPoolTaskExecutor.setAwaitTerminationSeconds(awaitTerminationSeconds);

        threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(waitForTasksToCompleteOnShutdown);
        threadPoolTaskExecutor.setDaemon(daemon);

        threadPoolTaskExecutor.initialize();

        return threadPoolTaskExecutor;
    }

    public void shutdownThreadPool(ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        if (threadPoolTaskExecutor != null) {
            threadPoolTaskExecutor.shutdown();
        }
    }
}
