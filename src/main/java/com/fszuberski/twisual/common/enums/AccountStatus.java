package com.fszuberski.twisual.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountStatus {
    UNKNOWN(0),
    ACTIVE(1),
    INACTIVE(2),
    BLOCKED(3);

    private int id;

    public static AccountStatus fromId(int id)
    {
        for (AccountStatus singleValue : values())
        {
            if (singleValue.id == id)
            {
                return singleValue;
            }
        }

        return UNKNOWN;
    }
}
