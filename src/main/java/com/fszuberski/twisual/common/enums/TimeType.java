package com.fszuberski.twisual.common.enums;

import lombok.Getter;

public enum TimeType {
    UNKNOWN(0),
    SECOND(1),
    MINUTE(2),
    HALF_HOUR(3);

    @Getter
    private int id;

    TimeType(int id) {
        this.id = id;
    }

    public static TimeType oneLower(TimeType timeType) {
        switch (timeType) {
            case HALF_HOUR:
                return MINUTE;
            case MINUTE:
                return SECOND;
            default:
                return UNKNOWN;
        }
    }
}
