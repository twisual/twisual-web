package com.fszuberski.twisual.common.manager;

import com.fszuberski.twisual.common.util.ThreadPoolFactory;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ThreadPoolManager {

    private ThreadPoolFactory threadPoolFactory;

    @Getter
    private ThreadPoolTaskExecutor asyncThreadPool;

    @Autowired
    public void setThreadPoolFactory(ThreadPoolFactory threadPoolFactory) {
        this.threadPoolFactory = threadPoolFactory;
    }

    public ThreadPoolTaskExecutor createAsyncThreadPool() {
        if (asyncThreadPool == null) {
            try {
                asyncThreadPool = threadPoolFactory.createThreadPoolTaskExecutor(
                        "ASYNC",
                        10,
                        20,
                        500,
                        3600,
                        120,
                        true,
                        false
                );

            } catch (Exception e) {
                log.warn("Exception while creating async thread pool", e);
                throw new IllegalStateException("Async could not be created");
            }
        }

        return asyncThreadPool;
    }

    public void shutdownAsynchronousExecutorPool() {
        threadPoolFactory.shutdownThreadPool(asyncThreadPool);
    }
}
