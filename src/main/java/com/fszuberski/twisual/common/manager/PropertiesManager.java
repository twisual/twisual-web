package com.fszuberski.twisual.common.manager;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@PropertySources({
        @PropertySource(value = "classpath:application.properties"),
        @PropertySource(value = "classpath:application-${spring.profiles.active}.properties", ignoreResourceNotFound = true)
})
@Configuration
public class PropertiesManager {

    //------------------------------------------------------------------------------------------------------------------ database

    @Getter
    @Value("${spring.data.mongodb.host}")
    private String dbHost;

    @Getter
    @Value("${spring.data.mongodb.port}")
    private Integer dbPort;

    @Getter
    @Value("${spring.data.mongodb.database}")
    private String dbDatabase;

    @Getter
    @Value("${spring.data.mongodb.map-dot-replacement}")
    private String dbDotReplacement;
}
