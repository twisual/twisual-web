package utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LogUtils {

    public static final SimpleDateFormat sdf = new SimpleDateFormat("hh:MM:ss.SSS");

    public static void log(Object message) {
        System.out.println("[" + currentTimeFormatted() + " : " + Thread.currentThread().getName() + "]: " + message);
    }

    public static String currentTimeFormatted() {
        return sdf.format(new Date());
    }
}
