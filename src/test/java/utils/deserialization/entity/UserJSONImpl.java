package utils.deserialization.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import twitter4j.Status;
import twitter4j.URLEntity;
import twitter4j.User;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
public class UserJSONImpl extends TwitterResponseImpl implements User {

    private static final long serialVersionUID = -5448266606847617015L;

    @JsonProperty("_id")
    private long id;
    private String name;
    private String email;
    private String screenName;
    private String location;
    private String description;
    private URLEntity[] descriptionURLEntities;
    private URLEntity urlEntity;
    private boolean isContributorsEnabled;
    private String profileImageUrl;
    private String profileImageUrlHttps;
    private boolean isDefaultProfileImage;
    private String url;
    private boolean isProtected;
    private int followersCount;

    private Status status;

    private String profileBackgroundColor;
    private String profileTextColor;
    private String profileLinkColor;
    private String profileSidebarFillColor;
    private String profileSidebarBorderColor;
    private boolean profileUseBackgroundImage;
    private boolean isDefaultProfile;
    private boolean showAllInlineMedia;
    private int friendsCount;
    private Date createdAt;
    private int favouritesCount;
    private int utcOffset;
    private String timeZone;
    private String profileBackgroundImageUrl;
    private String profileBackgroundImageUrlHttps;
    private String profileBannerImageUrl;
    private boolean profileBackgroundTiled;
    private String lang;
    private int statusesCount;
    private boolean isGeoEnabled;
    private boolean isVerified;
    private boolean translator;
    private int listedCount;
    private boolean isFollowRequestSent;
    private String[] withheldInCountries;

    @Override
    public int compareTo(User that) {
        return (int) (this.id - that.getId());
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getScreenName() {
        return screenName;
    }

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean isContributorsEnabled() {
        return isContributorsEnabled;
    }

    @Override
    public String getProfileImageURL() {
        return profileImageUrl;
    }

    @Override
    public String getBiggerProfileImageURL() {
        return toResizedURL(profileImageUrl, "_bigger");
    }

    @Override
    public String getMiniProfileImageURL() {
        return toResizedURL(profileImageUrl, "_mini");
    }

    @Override
    public String getOriginalProfileImageURL() {
        return toResizedURL(profileImageUrl, "");
    }

    private String toResizedURL(String originalURL, String sizeSuffix) {
        if (null != originalURL) {
            int index = originalURL.lastIndexOf("_");
            int suffixIndex = originalURL.lastIndexOf(".");
            int slashIndex = originalURL.lastIndexOf("/");
            String url = originalURL.substring(0, index) + sizeSuffix;
            if (suffixIndex > slashIndex) {
                url += originalURL.substring(suffixIndex);
            }
            return url;
        }
        return null;
    }

    @Override
    public String getProfileImageURLHttps() {
        return profileImageUrlHttps;
    }

    @Override
    public String getBiggerProfileImageURLHttps() {
        return toResizedURL(profileImageUrlHttps, "_bigger");
    }

    @Override
    public String getMiniProfileImageURLHttps() {
        return toResizedURL(profileImageUrlHttps, "_mini");
    }

    @Override
    public String getOriginalProfileImageURLHttps() {
        return toResizedURL(profileImageUrlHttps, "");
    }

    @Override
    public boolean isDefaultProfileImage() {
        return isDefaultProfileImage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getURL() {
        return url;
    }

    @Override
    public boolean isProtected() {
        return isProtected;
    }

    @Override
    public int getFollowersCount() {
        return followersCount;
    }

    @Override
    public String getProfileBackgroundColor() {
        return profileBackgroundColor;
    }

    @Override
    public String getProfileTextColor() {
        return profileTextColor;
    }

    @Override
    public String getProfileLinkColor() {
        return profileLinkColor;
    }

    @Override
    public String getProfileSidebarFillColor() {
        return profileSidebarFillColor;
    }

    @Override
    public String getProfileSidebarBorderColor() {
        return profileSidebarBorderColor;
    }

    @Override
    public boolean isProfileUseBackgroundImage() {
        return profileUseBackgroundImage;
    }

    @Override
    public boolean isDefaultProfile() {
        return isDefaultProfile;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isShowAllInlineMedia() {
        return showAllInlineMedia;
    }

    @Override
    public int getFriendsCount() {
        return friendsCount;
    }

    @Override
    public Status getStatus() {
        return status;
    }


    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public int getFavouritesCount() {
        return favouritesCount;
    }

    @Override
    public int getUtcOffset() {
        return utcOffset;
    }

    @Override
    public String getTimeZone() {
        return timeZone;
    }

    @Override
    public String getProfileBackgroundImageURL() {
        return profileBackgroundImageUrl;
    }

    @Override
    public String getProfileBackgroundImageUrlHttps() {
        return profileBackgroundImageUrlHttps;
    }

    @Override
    public String getProfileBannerURL() {
        return profileBannerImageUrl != null ? profileBannerImageUrl + "/web" : null;
    }

    @Override
    public String getProfileBannerRetinaURL() {
        return profileBannerImageUrl != null ? profileBannerImageUrl + "/web_retina" : null;
    }

    @Override
    public String getProfileBannerIPadURL() {
        return profileBannerImageUrl != null ? profileBannerImageUrl + "/ipad" : null;
    }

    @Override
    public String getProfileBannerIPadRetinaURL() {
        return profileBannerImageUrl != null ? profileBannerImageUrl + "/ipad_retina" : null;
    }

    @Override
    public String getProfileBannerMobileURL() {
        return profileBannerImageUrl != null ? profileBannerImageUrl + "/mobile" : null;
    }

    @Override
    public String getProfileBannerMobileRetinaURL() {
        return profileBannerImageUrl != null ? profileBannerImageUrl + "/mobile_retina" : null;
    }

    @Override
    public boolean isProfileBackgroundTiled() {
        return profileBackgroundTiled;
    }

    @Override
    public String getLang() {
        return lang;
    }

    @Override
    public int getStatusesCount() {
        return statusesCount;
    }

    @Override
    public boolean isGeoEnabled() {
        return isGeoEnabled;
    }

    @Override
    public boolean isVerified() {
        return isVerified;
    }

    @Override
    public boolean isTranslator() {
        return translator;
    }

    @Override
    public int getListedCount() {
        return listedCount;
    }

    @Override
    public boolean isFollowRequestSent() {
        return isFollowRequestSent;
    }

    @Override
    public URLEntity[] getDescriptionURLEntities() {
        return descriptionURLEntities;
    }

    @Override
    public URLEntity getURLEntity() {
        if (urlEntity == null) {
            String plainURL = url == null ? "" : url;
            urlEntity = new URLEntityJSONImpl(0, plainURL.length(), plainURL, plainURL, plainURL);
        }
        return urlEntity;
    }

    @Override
    public String[] getWithheldInCountries() {
        return withheldInCountries;
    }
}
