package utils.deserialization.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import twitter4j.*;

import java.util.Date;

@SuppressWarnings("ALL")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = false)
public class TwitterStatus extends TwitterResponseImpl implements Status {

    protected String id;
    protected Date createdAt;
    protected long twitter_id;
    protected String text;
    protected int displayTextRangeStart = -1;
    protected int displayTextRangeEnd = -1;
    protected String source;
    protected long inReplyToStatusId;
    protected long inReplyToUserId;
    protected int favoriteCount;
    protected String inReplyToScreenName;
    protected GeoLocation geoLocation = null;
    protected long retweetCount;
    protected String lang;
    protected long[] contributorsIDs;
    protected SymbolEntity[] symbolEntities;
    protected long currentUserRetweetId = -1L;
    protected Scopes scopes;
    protected String[] withheldInCountries = null;
    protected long quotedStatusId = -1L;

    public void setId(long id) {
        this.twitter_id = id;
    }

    @JsonProperty("isTruncated")
    protected boolean isTruncated;

    @JsonProperty("isFavorited")
    protected boolean isFavorited;

    @JsonProperty("isRetweeted")
    protected boolean isRetweeted;

    @JsonProperty("isPossiblySensitive")
    protected boolean isPossiblySensitive;

    @JsonDeserialize(as = MediaEntityJSONImpl[].class)
    protected MediaEntity[] mediaEntities;

    @JsonDeserialize(as = URLEntityJSONImpl[].class)
    private URLEntity[] urlEntities;

    @JsonDeserialize(as = UserMentionEntityJSONImpl[].class)
    protected UserMentionEntity[] userMentionEntities;

    @JsonDeserialize(as = HashtagEntityJSONImpl[].class)
    protected HashtagEntity[] hashtagEntities;

    @JsonDeserialize(as = UserJSONImpl.class)
    private User user;

    @JsonDeserialize(as = TwitterStatus.class)
    protected Status retweetedStatus;

    @JsonDeserialize(as = TwitterStatus.class)
    protected Status quotedStatus;

    @JsonDeserialize(as = PlaceJSONImpl.class)
    protected Place place = null;

    @Override
    public long getId() {
        return twitter_id;
    }

    @Override
    public int getRetweetCount() {
        return (int) retweetCount;
    }

    @Override
    public boolean isRetweet() {
        return retweetedStatus != null;
    }

    @Override
    public long[] getContributors() {
        return contributorsIDs;
    }

    @Override
    public boolean isRetweetedByMe() {
        return currentUserRetweetId != -1L;
    }

    @Override
    public URLEntity[] getURLEntities() {
        return urlEntities;
    }

    @Override
    public int compareTo(Status that) {
        long delta = this.twitter_id - that.getId();
        if (delta < Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        } else if (delta > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }
        return (int) delta;
    }
}
