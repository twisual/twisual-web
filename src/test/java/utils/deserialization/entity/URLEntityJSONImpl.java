package utils.deserialization.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import twitter4j.URLEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class URLEntityJSONImpl extends EntityIndex implements URLEntity {

    private String url;
    private String expandedURL;
    private String displayURL;

    /* package */ URLEntityJSONImpl(int start, int end, String url, String expandedURL, String displayURL) {
        super();
        setStart(start);
        setEnd(end);
        this.url = url;
        this.expandedURL = expandedURL;
        this.displayURL = displayURL;
    }

    @Override
    public String getText() {
        return url;
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public String getExpandedURL() {
        return expandedURL;
    }

    @Override
    public String getDisplayURL() {
        return displayURL;
    }

    @Override
    public int getStart() {
        return super.getStart();
    }

    @Override
    public int getEnd() {
        return super.getEnd();
    }
}
