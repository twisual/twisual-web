package utils.deserialization.entity;

import twitter4j.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yusuke Yamamoto - yusuke at mac.com
 * @since Twitter4J 2.2.3
 */
public class MediaEntityJSONImpl extends EntityIndex implements MediaEntity {
    private static final long serialVersionUID = 1571961225214439778L;
    protected long id;
    protected String url;
    private String mediaURL;
    private String mediaURLHttps;
    private String expandedURL;
    private String displayURL;
    private Map<Integer, Size> sizes;
    protected String type;
    private int videoAspectRatioWidth;
    private int videoAspectRatioHeight;
    private long videoDurationMillis;
    private Variant[] videoVariants;
    private String extAltText;


    MediaEntityJSONImpl(JSONObject json) throws TwitterException {
        try {
            JSONArray indicesArray = json.getJSONArray("indices");
            setStart(indicesArray.getInt(0));
            setEnd(indicesArray.getInt(1));
            this.id = ParseUtil.getLong("id", json);

            this.url = json.getString("url");
            this.expandedURL = json.getString("expanded_url");
            this.mediaURL = json.getString("media_url");
            this.mediaURLHttps = json.getString("media_url_https");
            this.displayURL = json.getString("display_url");

            JSONObject sizes = json.getJSONObject("sizes");
            this.sizes = new HashMap<>(4);
            // thumbworkarounding API side issue
            addMediaEntitySizeIfNotNull(this.sizes, sizes, MediaEntity.Size.LARGE, "large");
            addMediaEntitySizeIfNotNull(this.sizes, sizes, MediaEntity.Size.MEDIUM, "medium");
            addMediaEntitySizeIfNotNull(this.sizes, sizes, MediaEntity.Size.SMALL, "small");
            addMediaEntitySizeIfNotNull(this.sizes, sizes, MediaEntity.Size.THUMB, "thumb");
            if (!json.isNull("type")) {
                this.type = json.getString("type");
            }

            if (json.has("video_info")) {
                JSONObject videoInfo = json.getJSONObject("video_info");
                JSONArray aspectRatio = videoInfo.getJSONArray("aspect_ratio");
                this.videoAspectRatioWidth = aspectRatio.getInt(0);
                this.videoAspectRatioHeight = aspectRatio.getInt(1);

                // not in animated_gif
                if (!videoInfo.isNull("duration_millis")) {
                    this.videoDurationMillis = videoInfo.getLong("duration_millis");
                }

                JSONArray variants = videoInfo.getJSONArray("variants");
                this.videoVariants = new MediaEntityJSONImpl.Variant[variants.length()];
                for (int i = 0; i < variants.length(); i++) {
                    this.videoVariants[i] = new MediaEntityJSONImpl.Variant(variants.getJSONObject(i));
                }
            } else {
                this.videoVariants = new MediaEntityJSONImpl.Variant[0];
            }

            if (json.has("ext_alt_text")) {
                extAltText = json.getString("ext_alt_text");
            }

        } catch (JSONException jsone) {
            throw new TwitterException(jsone);
        }
    }

    private void addMediaEntitySizeIfNotNull(Map<Integer, Size> sizes, JSONObject sizesJSON, Integer size, String key) throws JSONException {
        if (!sizesJSON.isNull(key)) {
            sizes.put(size, new MediaEntityJSONImpl.Size(sizesJSON.getJSONObject(key)));
        }
    }

    /* For serialization purposes only. */
    /* package */ MediaEntityJSONImpl() {

    }


    public long getId() {
        return id;
    }


    public String getMediaURL() {
        return mediaURL;
    }


    public String getMediaURLHttps() {
        return mediaURLHttps;
    }


    public String getText() {
        return url;
    }


    public String getURL() {
        return url;
    }


    public String getDisplayURL() {
        return displayURL;
    }


    public String getExpandedURL() {
        return expandedURL;
    }


    public Map<Integer, MediaEntity.Size> getSizes() {
        return null;
    }


    public String getType() {
        return type;
    }


    public int getStart() {
        return super.getStart();
    }


    public int getEnd() {
        return super.getEnd();
    }

    static class Size implements MediaEntity.Size {
        private static final long serialVersionUID = -2515842281909325169L;
        int width;
        int height;
        int resize;

        public static Integer THUMB = 0;
        public static Integer SMALL = 1;
        public static Integer MEDIUM = 2;
        public static Integer LARGE = 3;
        int FIT = 100;
        int CROP = 101;

        /* For serialization purposes only. */
        /* package */
        Size() {
        }

        Size(JSONObject json) throws JSONException {
            width = json.getInt("w");
            height = json.getInt("h");
            resize = "fit".equals(json.getString("resize")) ? FIT : CROP;
        }


        public int getWidth() {
            return width;
        }


        public int getHeight() {
            return height;
        }


        public int getResize() {
            return resize;
        }


        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof MediaEntityJSONImpl.Size)) return false;

            MediaEntityJSONImpl.Size size = (MediaEntityJSONImpl.Size) o;

            if (height != size.height) return false;
            if (resize != size.resize) return false;
            if (width != size.width) return false;

            return true;
        }


        public int hashCode() {
            int result = width;
            result = 31 * result + height;
            result = 31 * result + resize;
            return result;
        }


        public String toString() {
            return "Size{" +
                    "width=" + width +
                    ", height=" + height +
                    ", resize=" + resize +
                    '}';
        }
    }


    public int getVideoAspectRatioWidth() {
        return videoAspectRatioWidth;
    }


    public int getVideoAspectRatioHeight() {
        return videoAspectRatioHeight;
    }


    public long getVideoDurationMillis() {
        return videoDurationMillis;
    }


    public String getExtAltText() {
        return extAltText;
    }


    public MediaEntity.Variant[] getVideoVariants() {
        return null;
    }

    static class Variant {
        private static final long serialVersionUID = 1027236588556797980L;
        int bitrate;
        String contentType;
        String url;

        Variant(JSONObject json) throws JSONException {
            bitrate = json.has("bitrate") ? json.getInt("bitrate") : 0;
            contentType = json.getString("content_type");
            url = json.getString("url");
        }

        /* For serialization purposes only. */
        /* package */ Variant() {
        }


        public int getBitrate() {
            return bitrate;
        }


        public String getContentType() {
            return contentType;
        }


        public String getUrl() {
            return url;
        }


        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof MediaEntityJSONImpl.Variant)) return false;

            MediaEntityJSONImpl.Variant variant = (MediaEntityJSONImpl.Variant) o;

            if (bitrate != variant.bitrate) return false;
            if (!contentType.equals(variant.contentType)) return false;
            if (!url.equals(variant.url)) return false;

            return true;
        }


        public int hashCode() {
            int result = bitrate;
            result = 31 * result + (contentType != null ? contentType.hashCode() : 0);
            result = 31 * result + (url != null ? url.hashCode() : 0);
            return result;
        }


        public String toString() {
            return "Variant{" +
                    "bitrate=" + bitrate +
                    ", contentType=" + contentType +
                    ", url=" + url +
                    '}';
        }
    }


    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof twitter4j.MediaEntityJSONImpl)) return false;

        MediaEntityJSONImpl that = (MediaEntityJSONImpl) o;

        if (id != that.id) return false;

        return true;
    }


    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }


    public String toString() {
        return "MediaEntityJSONImpl{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", mediaURL='" + mediaURL + '\'' +
                ", mediaURLHttps='" + mediaURLHttps + '\'' +
                ", expandedURL='" + expandedURL + '\'' +
                ", displayURL='" + displayURL + '\'' +
                ", sizes=" + sizes +
                ", type='" + type + '\'' +
                ", videoAspectRatioWidth=" + videoAspectRatioWidth +
                ", videoAspectRatioHeight=" + videoAspectRatioHeight +
                ", videoDurationMillis=" + videoDurationMillis +
                ", videoVariants=" + videoVariants.length +
                ", extAltText='" + extAltText + '\'' +
                '}';
    }
}