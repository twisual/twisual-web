package utils.deserialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fszuberski.twisual.data.entity.TwitterStatusStatistics;
import com.fszuberski.twisual.data.entity.TwitterUserStatistics;

import java.io.File;

public class StatisticsGenerator {

    private static ObjectMapper objectMapper;

    public static TwitterStatusStatistics singleStatusStatistics(int fileNumber) {
        TwitterStatusStatistics twitterStatusStatistics = null;

        try {
            File file = new File(StatisticsGenerator.class
                    .getResource("/statistics/status/single/test_single_status_statistics_" + fileNumber + ".json").getPath());

            twitterStatusStatistics = getObjectMapper().readValue(file, TwitterStatusStatistics.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return twitterStatusStatistics;
    }

    public static TwitterUserStatistics singleUserStatistics(int fileNumber) {
        TwitterUserStatistics twitterUserStatistics = null;

        try {
            File file = new File(StatisticsGenerator.class
                    .getResource("/statistics/user/single/test_single_user_statistics_" + fileNumber + ".json").getPath());

            twitterUserStatistics = getObjectMapper().readValue(file, TwitterUserStatistics.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return twitterUserStatistics;
    }

    private static ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }

        return objectMapper;
    }
}
