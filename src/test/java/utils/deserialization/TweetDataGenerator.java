package utils.deserialization;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fszuberski.twisual.data.entity.TwitterStatus;
import com.fszuberski.twisual.data.entity.TwitterUser;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class TweetDataGenerator {

    private static ObjectMapper objectMapper;

    public static List<TwitterStatus> testMultipleTweets(int fileNumber) {
        List<TwitterStatus> twitterStatusList = new LinkedList<>();

        try {
            File file = new File(TweetDataGenerator.class
                    .getResource("/tweets/multiple/test_multiple_tweets_" + fileNumber + ".json")
                    .getPath());

            List<utils.deserialization.entity.TwitterStatus> testTwitterStatusList = getObjectMapper()
                    .readValue(file, new TypeReference<List<utils.deserialization.entity.TwitterStatus>>() {});

            for (utils.deserialization.entity.TwitterStatus singleTwitterStatus : testTwitterStatusList) {
                TwitterStatus realTwitterStatus = convertTwitterStatus(singleTwitterStatus);
                twitterStatusList.add(realTwitterStatus);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return twitterStatusList;
    }

    public static TwitterStatus testSingleTweet(int fileNumber) {
        TwitterStatus twitterStatus = null;

        try {
            File file = new File(TweetDataGenerator.class
                    .getResource("/tweets/single/test_single_tweet_" + fileNumber + ".json")
                    .getPath());

            utils.deserialization.entity.TwitterStatus testTwitterStatus = getObjectMapper()
                    .readValue(file, utils.deserialization.entity.TwitterStatus.class);

            twitterStatus = convertTwitterStatus(testTwitterStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return twitterStatus;
    }

    public static TwitterUser testSingleUser(int fileNumber) {
        TwitterUser twitterUser = null;

        try {
            File file = new File(TweetDataGenerator.class
                    .getResource("/users/single/test_single_user_" + fileNumber + ".json")
                    .getPath());

            twitterUser = getObjectMapper().readValue(file, TwitterUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return twitterUser;
    }

    private static TwitterStatus convertTwitterStatus(utils.deserialization.entity.TwitterStatus singleTwitterStatus) {
        return TwitterStatus
                .builder()
                .id(String.valueOf(singleTwitterStatus.getId()))
                .createdAt(singleTwitterStatus.getCreatedAt())
                .twitter_id(singleTwitterStatus.getId())
                .text(singleTwitterStatus.getText())
                .displayTextRangeStart(singleTwitterStatus.getDisplayTextRangeStart())
                .displayTextRangeEnd(singleTwitterStatus.getDisplayTextRangeEnd())
                .source(singleTwitterStatus.getSource())
                .isTruncated(singleTwitterStatus.isTruncated())
                .inReplyToStatusId(singleTwitterStatus.getInReplyToStatusId())
                .inReplyToUserId(singleTwitterStatus.getInReplyToUserId())
                .isFavorited(singleTwitterStatus.isFavorited())
                .isRetweeted(singleTwitterStatus.isRetweeted())
                .favoriteCount(singleTwitterStatus.getFavoriteCount())
                .inReplyToScreenName(singleTwitterStatus.getInReplyToScreenName())
                .geoLocation(singleTwitterStatus.getGeoLocation())
                .place(singleTwitterStatus.getPlace())
                .retweetCount(singleTwitterStatus.getRetweetCount())
                .isPossiblySensitive(singleTwitterStatus.isPossiblySensitive())
                .lang(singleTwitterStatus.getLang())
                .contributorsIDs(singleTwitterStatus.getContributors())
                .retweetedStatus(singleTwitterStatus.getRetweetedStatus())
                .userMentionEntities(singleTwitterStatus.getUserMentionEntities())
                .urlEntities(singleTwitterStatus.getURLEntities())
                .hashtagEntities(singleTwitterStatus.getHashtagEntities())
                .mediaEntities(singleTwitterStatus.getMediaEntities())
                .symbolEntities(singleTwitterStatus.getSymbolEntities())
                .currentUserRetweetId(singleTwitterStatus.getCurrentUserRetweetId())
                .scopes(singleTwitterStatus.getScopes())
                .user(singleTwitterStatus.getUser())
                .withheldInCountries(singleTwitterStatus.getWithheldInCountries())
                .quotedStatus(singleTwitterStatus.getQuotedStatus())
                .quotedStatusId(singleTwitterStatus.getQuotedStatusId())
                .build();
    }

    private static ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }

        return objectMapper;
    }
}
