package com.fszuberski.twisual.business.control.merge;

import com.fszuberski.twisual.data.entity.GeoLocation;
import com.fszuberski.twisual.data.entity.TwitterStatusStatistics;
import com.fszuberski.twisual.stream.entity.Place;
import org.junit.Before;
import org.junit.Test;
import utils.deserialization.StatisticsGenerator;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@SuppressWarnings("Duplicates")
public class TwitterStatusStatisticsMergerTest {

    private TwitterStatusStatisticsMerger twitterStatusStatisticsMerger;

    @Before
    public void before() {
        twitterStatusStatisticsMerger = new TwitterStatusStatisticsMerger();
    }

    //------------------------------------------------------------------------------------------------------------------ merge

    @Test
    public void mergeTest_1() {
        TwitterStatusStatistics statistics1 = StatisticsGenerator.singleStatusStatistics(1);
        TwitterStatusStatistics testStatistics1 = StatisticsGenerator.singleStatusStatistics(1);

        TwitterStatusStatistics mergedStatistics = twitterStatusStatisticsMerger.merge(testStatistics1, testStatistics1);

        assertNotNull(mergedStatistics);
        assertEquals(statistics1.getTweetCount() * 2, mergedStatistics.getTweetCount());
        assertEquals(statistics1.getTruncatedCount() * 2, mergedStatistics.getTruncatedCount());
        assertEquals(statistics1.getFavoritedCount() * 2, mergedStatistics.getFavoritedCount());
        assertEquals(statistics1.getRetweetedCount() * 2, mergedStatistics.getRetweetedCount());
        assertEquals(statistics1.getPossiblySensitiveCount() * 2, mergedStatistics.getPossiblySensitiveCount());
        assertEquals(statistics1.getUserMentionEntityCount() * 2, mergedStatistics.getUserMentionEntityCount());
        assertEquals(statistics1.getUrlEntityCount() * 2, mergedStatistics.getUrlEntityCount());
        assertEquals(statistics1.getHashtagEntityCount() * 2, mergedStatistics.getHashtagEntityCount());
        assertEquals(statistics1.getMediaEntityCount() * 2, mergedStatistics.getMediaEntityCount());
        assertEquals(statistics1.getSymbolEntityCount() * 2, mergedStatistics.getSymbolEntityCount());

        assertEquals(statistics1.getGeoLocations().size() * 2, mergedStatistics.getGeoLocations().size());
        assertEquals(statistics1.getPlaces().size() * 2, mergedStatistics.getPlaces().size());

        for (GeoLocation geoLocation : statistics1.getGeoLocations()) {
            assertTrue(mergedStatistics.getGeoLocations().contains(geoLocation));
        }

        for (Place place : statistics1.getPlaces()) {
            assertTrue(mergedStatistics.getPlaces().contains(place));
        }

        for (Map.Entry<String, Integer> sourcesEntry : statistics1.getSources().entrySet()) {
            assertTrue(mergedStatistics.getSources().containsKey(sourcesEntry.getKey()));
            assertNotNull(mergedStatistics.getSources().get(sourcesEntry.getKey()));
            assertEquals(sourcesEntry.getValue() * 2, (int) mergedStatistics.getSources().get(sourcesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> languagesEntry : statistics1.getLanguages().entrySet()) {
            assertTrue(mergedStatistics.getLanguages().containsKey(languagesEntry.getKey()));
            assertNotNull(mergedStatistics.getLanguages().get(languagesEntry.getKey()));
            assertEquals(languagesEntry.getValue() * 2, (int) mergedStatistics.getLanguages().get(languagesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> userMentionEntitiesEntry : statistics1.getUserMentionEntities().entrySet()) {
            assertTrue(mergedStatistics.getUserMentionEntities().containsKey(userMentionEntitiesEntry.getKey()));
            assertNotNull(mergedStatistics.getUserMentionEntities().get(userMentionEntitiesEntry.getKey()));
            assertEquals(userMentionEntitiesEntry.getValue() * 2, (int) mergedStatistics.getUserMentionEntities().get(userMentionEntitiesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> urlEntitiesEntry : statistics1.getUrlEntities().entrySet()) {
            assertTrue(mergedStatistics.getUrlEntities().containsKey(urlEntitiesEntry.getKey()));
            assertNotNull(mergedStatistics.getUrlEntities().get(urlEntitiesEntry.getKey()));
            assertEquals(urlEntitiesEntry.getValue() * 2, (int) mergedStatistics.getUrlEntities().get(urlEntitiesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> hashtagEntitiesEntry : statistics1.getHashtagEntities().entrySet()) {
            assertTrue(mergedStatistics.getHashtagEntities().containsKey(hashtagEntitiesEntry.getKey()));
            assertNotNull(mergedStatistics.getHashtagEntities().get(hashtagEntitiesEntry.getKey()));
            assertEquals(hashtagEntitiesEntry.getValue() * 2, (int) mergedStatistics.getHashtagEntities().get(hashtagEntitiesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> mediaEntityTypesEntry : statistics1.getMediaEntityTypes().entrySet()) {
            assertTrue(mergedStatistics.getMediaEntityTypes().containsKey(mediaEntityTypesEntry.getKey()));
            assertNotNull(mergedStatistics.getMediaEntityTypes().get(mediaEntityTypesEntry.getKey()));
            assertEquals(mediaEntityTypesEntry.getValue() * 2, (int) mergedStatistics.getMediaEntityTypes().get(mediaEntityTypesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> mediaEntityUrlsEntry : statistics1.getMediaEntityUrls().entrySet()) {
            assertTrue(mergedStatistics.getMediaEntityUrls().containsKey(mediaEntityUrlsEntry.getKey()));
            assertNotNull(mergedStatistics.getMediaEntityUrls().get(mediaEntityUrlsEntry.getKey()));
            assertEquals(mediaEntityUrlsEntry.getValue() * 2, (int) mergedStatistics.getMediaEntityUrls().get(mediaEntityUrlsEntry.getKey()));
        }

        for (Map.Entry<String, Integer> symbolEntitiesTextsEntry : statistics1.getSymbolEntityTexts().entrySet()) {
            assertTrue(mergedStatistics.getSymbolEntityTexts().containsKey(symbolEntitiesTextsEntry.getKey()));
            assertNotNull(mergedStatistics.getSymbolEntityTexts().get(symbolEntitiesTextsEntry.getKey()));
            assertEquals(symbolEntitiesTextsEntry.getValue() * 2, (int) mergedStatistics.getSymbolEntityTexts().get(symbolEntitiesTextsEntry.getKey()));
        }

        for (Map.Entry<String, Integer> placeTypesEntry : statistics1.getPlaceTypes().entrySet()) {
            assertTrue(mergedStatistics.getPlaceTypes().containsKey(placeTypesEntry.getKey()));
            assertNotNull(mergedStatistics.getPlaceTypes().get(placeTypesEntry.getKey()));
            assertEquals(placeTypesEntry.getValue() * 2, (int) mergedStatistics.getPlaceTypes().get(placeTypesEntry.getKey()));
        }

        for (Map.Entry<String, HashMap<String, Integer>> wordLanguageEntry : statistics1.getWords().entrySet()) {
            assertTrue(mergedStatistics.getWords().containsKey(wordLanguageEntry.getKey()));
            assertNotNull(mergedStatistics.getWords().get(wordLanguageEntry.getKey()));

            for (Map.Entry<String, Integer> wordWordEntry : wordLanguageEntry.getValue().entrySet()) {
                assertTrue(mergedStatistics.getWords().get(wordLanguageEntry.getKey()).containsKey(wordWordEntry.getKey()));
                assertNotNull(mergedStatistics.getWords().get(wordLanguageEntry.getKey()).get(wordWordEntry.getKey()));
                assertEquals(wordWordEntry.getValue() * 2, (int) mergedStatistics.getWords().get(wordLanguageEntry.getKey()).get(wordWordEntry.getKey()));
            }
        }

    }

    @Test
    public void mergeTest_2() {
        TwitterStatusStatistics statistics2 = StatisticsGenerator.singleStatusStatistics(2);
        TwitterStatusStatistics testStatistics2 = StatisticsGenerator.singleStatusStatistics(2);

        TwitterStatusStatistics mergedStatistics = twitterStatusStatisticsMerger.merge(testStatistics2, testStatistics2);

        assertNotNull(mergedStatistics);
        assertEquals(statistics2.getTweetCount() * 2, mergedStatistics.getTweetCount());
        assertEquals(statistics2.getTruncatedCount() * 2, mergedStatistics.getTruncatedCount());
        assertEquals(statistics2.getFavoritedCount() * 2, mergedStatistics.getFavoritedCount());
        assertEquals(statistics2.getRetweetedCount() * 2, mergedStatistics.getRetweetedCount());
        assertEquals(statistics2.getPossiblySensitiveCount() * 2, mergedStatistics.getPossiblySensitiveCount());
        assertEquals(statistics2.getUserMentionEntityCount() * 2, mergedStatistics.getUserMentionEntityCount());
        assertEquals(statistics2.getUrlEntityCount() * 2, mergedStatistics.getUrlEntityCount());
        assertEquals(statistics2.getHashtagEntityCount() * 2, mergedStatistics.getHashtagEntityCount());
        assertEquals(statistics2.getMediaEntityCount() * 2, mergedStatistics.getMediaEntityCount());
        assertEquals(statistics2.getSymbolEntityCount() * 2, mergedStatistics.getSymbolEntityCount());

        assertEquals(statistics2.getGeoLocations().size() * 2, mergedStatistics.getGeoLocations().size());
        assertEquals(statistics2.getPlaces().size() * 2, mergedStatistics.getPlaces().size());

        for (GeoLocation geoLocation : statistics2.getGeoLocations()) {
            assertTrue(mergedStatistics.getGeoLocations().contains(geoLocation));
        }

        for (Place place : statistics2.getPlaces()) {
            assertTrue(mergedStatistics.getPlaces().contains(place));
        }

        for (Map.Entry<String, Integer> sourcesEntry : statistics2.getSources().entrySet()) {
            assertTrue(mergedStatistics.getSources().containsKey(sourcesEntry.getKey()));
            assertNotNull(mergedStatistics.getSources().get(sourcesEntry.getKey()));
            assertEquals(sourcesEntry.getValue() * 2, (int) mergedStatistics.getSources().get(sourcesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> languagesEntry : statistics2.getLanguages().entrySet()) {
            assertTrue(mergedStatistics.getLanguages().containsKey(languagesEntry.getKey()));
            assertNotNull(mergedStatistics.getLanguages().get(languagesEntry.getKey()));
            assertEquals(languagesEntry.getValue() * 2, (int) mergedStatistics.getLanguages().get(languagesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> userMentionEntitiesEntry : statistics2.getUserMentionEntities().entrySet()) {
            assertTrue(mergedStatistics.getUserMentionEntities().containsKey(userMentionEntitiesEntry.getKey()));
            assertNotNull(mergedStatistics.getUserMentionEntities().get(userMentionEntitiesEntry.getKey()));
            assertEquals(userMentionEntitiesEntry.getValue() * 2, (int) mergedStatistics.getUserMentionEntities().get(userMentionEntitiesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> urlEntitiesEntry : statistics2.getUrlEntities().entrySet()) {
            assertTrue(mergedStatistics.getUrlEntities().containsKey(urlEntitiesEntry.getKey()));
            assertNotNull(mergedStatistics.getUrlEntities().get(urlEntitiesEntry.getKey()));
            assertEquals(urlEntitiesEntry.getValue() * 2, (int) mergedStatistics.getUrlEntities().get(urlEntitiesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> hashtagEntitiesEntry : statistics2.getHashtagEntities().entrySet()) {
            assertTrue(mergedStatistics.getHashtagEntities().containsKey(hashtagEntitiesEntry.getKey()));
            assertNotNull(mergedStatistics.getHashtagEntities().get(hashtagEntitiesEntry.getKey()));
            assertEquals(hashtagEntitiesEntry.getValue() * 2, (int) mergedStatistics.getHashtagEntities().get(hashtagEntitiesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> mediaEntityTypesEntry : statistics2.getMediaEntityTypes().entrySet()) {
            assertTrue(mergedStatistics.getMediaEntityTypes().containsKey(mediaEntityTypesEntry.getKey()));
            assertNotNull(mergedStatistics.getMediaEntityTypes().get(mediaEntityTypesEntry.getKey()));
            assertEquals(mediaEntityTypesEntry.getValue() * 2, (int) mergedStatistics.getMediaEntityTypes().get(mediaEntityTypesEntry.getKey()));
        }

        for (Map.Entry<String, Integer> mediaEntityUrlsEntry : statistics2.getMediaEntityUrls().entrySet()) {
            assertTrue(mergedStatistics.getMediaEntityUrls().containsKey(mediaEntityUrlsEntry.getKey()));
            assertNotNull(mergedStatistics.getMediaEntityUrls().get(mediaEntityUrlsEntry.getKey()));
            assertEquals(mediaEntityUrlsEntry.getValue() * 2, (int) mergedStatistics.getMediaEntityUrls().get(mediaEntityUrlsEntry.getKey()));
        }

        for (Map.Entry<String, Integer> symbolEntitiesTextsEntry : statistics2.getSymbolEntityTexts().entrySet()) {
            assertTrue(mergedStatistics.getSymbolEntityTexts().containsKey(symbolEntitiesTextsEntry.getKey()));
            assertNotNull(mergedStatistics.getSymbolEntityTexts().get(symbolEntitiesTextsEntry.getKey()));
            assertEquals(symbolEntitiesTextsEntry.getValue() * 2, (int) mergedStatistics.getSymbolEntityTexts().get(symbolEntitiesTextsEntry.getKey()));
        }

        for (Map.Entry<String, Integer> placeTypesEntry : statistics2.getPlaceTypes().entrySet()) {
            assertTrue(mergedStatistics.getPlaceTypes().containsKey(placeTypesEntry.getKey()));
            assertNotNull(mergedStatistics.getPlaceTypes().get(placeTypesEntry.getKey()));
            assertEquals(placeTypesEntry.getValue() * 2, (int) mergedStatistics.getPlaceTypes().get(placeTypesEntry.getKey()));
        }

        for (Map.Entry<String, HashMap<String, Integer>> wordLanguageEntry : statistics2.getWords().entrySet()) {
            assertTrue(mergedStatistics.getWords().containsKey(wordLanguageEntry.getKey()));
            assertNotNull(mergedStatistics.getWords().get(wordLanguageEntry.getKey()));

            for (Map.Entry<String, Integer> wordWordEntry : wordLanguageEntry.getValue().entrySet()) {
                assertTrue(mergedStatistics.getWords().get(wordLanguageEntry.getKey()).containsKey(wordWordEntry.getKey()));
                assertNotNull(mergedStatistics.getWords().get(wordLanguageEntry.getKey()).get(wordWordEntry.getKey()));
                assertEquals(wordWordEntry.getValue() * 2, (int) mergedStatistics.getWords().get(wordLanguageEntry.getKey()).get(wordWordEntry.getKey()));
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------------ mergeMaps

    @Test
    public void mergeMapsTest_1() {
        Map<String, Integer> map1 = new HashMap<>();
        map1.put("test1", 1);
        map1.put("test2", 2);
        map1.put("test3", 3);

        Map<String, Integer> map2 = new HashMap<>();
        map2.put("test4", 4);
        map2.put("test5", 5);
        map2.put("test6", 6);

        Map<String, Integer> mergedMap = twitterStatusStatisticsMerger.mergeMaps(map1, map2);

        assertNotNull(mergedMap.get("test1"));
        assertEquals(1, (int) mergedMap.get("test1"));

        assertNotNull(mergedMap.get("test2"));
        assertEquals(2, (int) mergedMap.get("test2"));

        assertNotNull(mergedMap.get("test3"));
        assertEquals(3, (int) mergedMap.get("test3"));

        assertNotNull(mergedMap.get("test4"));
        assertEquals(4, (int) mergedMap.get("test4"));

        assertNotNull(mergedMap.get("test5"));
        assertEquals(5, (int) mergedMap.get("test5"));

        assertNotNull(mergedMap.get("test6"));
        assertEquals(6, (int) mergedMap.get("test6"));
    }

    @Test
    public void mergeMapsTest_2() {
        Map<String, Integer> map1 = new HashMap<>();
        map1.put("test1", 1);
        map1.put("test2", 2);
        map1.put("test3", 3);

        Map<String, Integer> map2 = new HashMap<>();
        map2.put("test1", 4);
        map2.put("test2", 5);
        map2.put("test3", 6);

        Map<String, Integer> mergedMap = twitterStatusStatisticsMerger.mergeMaps(map1, map2);

        assertNotNull(mergedMap.get("test1"));
        assertEquals(5, (int) mergedMap.get("test1"));

        assertNotNull(mergedMap.get("test2"));
        assertEquals(7, (int) mergedMap.get("test2"));

        assertNotNull(mergedMap.get("test3"));
        assertEquals(9, (int) mergedMap.get("test3"));
    }

    @Test
    public void mergeMapsTest_3() {
        Map<String, Integer> map1 = new HashMap<>();
        map1.put("test1", 1);
        map1.put("test2", 2);
        map1.put("test3", 3);

        Map<String, Integer> map2 = new HashMap<>();

        Map<String, Integer> mergedMap = twitterStatusStatisticsMerger.mergeMaps(map1, map2);

        assertNotNull(mergedMap.get("test1"));
        assertEquals(1, (int) mergedMap.get("test1"));

        assertNotNull(mergedMap.get("test2"));
        assertEquals(2, (int) mergedMap.get("test2"));

        assertNotNull(mergedMap.get("test3"));
        assertEquals(3, (int) mergedMap.get("test3"));
    }

    @Test
    public void mergeMapsTest_4() {
        Map<String, Integer> map1 = new HashMap<>();

        Map<String, Integer> map2 = new HashMap<>();
        map2.put("test1", 4);
        map2.put("test2", 5);
        map2.put("test3", 6);

        Map<String, Integer> mergedMap = twitterStatusStatisticsMerger.mergeMaps(map1, map2);

        assertNotNull(mergedMap.get("test1"));
        assertEquals(4, (int) mergedMap.get("test1"));

        assertNotNull(mergedMap.get("test2"));
        assertEquals(5, (int) mergedMap.get("test2"));

        assertNotNull(mergedMap.get("test3"));
        assertEquals(6, (int) mergedMap.get("test3"));
    }

    @Test
    public void mergeMapsTest_5() {
        Map<Integer, Integer> map1 = new HashMap<>();
        map1.put(1, 1);
        map1.put(2, 2);
        map1.put(3, 3);

        Map<Integer, Integer> map2 = new HashMap<>();
        map2.put(4, 4);
        map2.put(5, 5);
        map2.put(6, 6);

        Map<Integer, Integer> mergedMap = twitterStatusStatisticsMerger.mergeMaps(map1, map2);

        assertNotNull(mergedMap.get(1));
        assertEquals(1, (int) mergedMap.get(1));

        assertNotNull(mergedMap.get(2));
        assertEquals(2, (int) mergedMap.get(2));

        assertNotNull(mergedMap.get(3));
        assertEquals(3, (int) mergedMap.get(3));

        assertNotNull(mergedMap.get(4));
        assertEquals(4, (int) mergedMap.get(4));

        assertNotNull(mergedMap.get(5));
        assertEquals(5, (int) mergedMap.get(5));

        assertNotNull(mergedMap.get(6));
        assertEquals(6, (int) mergedMap.get(6));
    }

    @Test
    public void mergeMapsTest_6() {
        Map<Integer, Integer> map1 = new HashMap<>();
        map1.put(1, 1);
        map1.put(2, 2);
        map1.put(3, 3);

        Map<Integer, Integer> map2 = new HashMap<>();
        map2.put(1, 4);
        map2.put(2, 5);
        map2.put(3, 6);

        Map<Integer, Integer> mergedMap = twitterStatusStatisticsMerger.mergeMaps(map1, map2);

        assertNotNull(mergedMap.get(1));
        assertEquals(5, (int) mergedMap.get(1));

        assertNotNull(mergedMap.get(2));
        assertEquals(7, (int) mergedMap.get(2));

        assertNotNull(mergedMap.get(3));
        assertEquals(9, (int) mergedMap.get(3));
    }

    @Test
    public void mergeMapsTest_7() {
        Map<Integer, Integer> map1 = new HashMap<>();
        map1.put(1, 1);
        map1.put(2, 2);
        map1.put(3, 3);

        Map<Integer, Integer> map2 = new HashMap<>();

        Map<Integer, Integer> mergedMap = twitterStatusStatisticsMerger.mergeMaps(map1, map2);

        assertNotNull(mergedMap.get(1));
        assertEquals(1, (int) mergedMap.get(1));

        assertNotNull(mergedMap.get(2));
        assertEquals(2, (int) mergedMap.get(2));

        assertNotNull(mergedMap.get(3));
        assertEquals(3, (int) mergedMap.get(3));
    }

    @Test
    public void mergeMapsTest_8() {
        Map<Integer, Integer> map1 = new HashMap<>();

        Map<Integer, Integer> map2 = new HashMap<>();
        map2.put(1, 4);
        map2.put(2, 5);
        map2.put(3, 6);

        Map<Integer, Integer> mergedMap = twitterStatusStatisticsMerger.mergeMaps(map1, map2);

        assertNotNull(mergedMap.get(1));
        assertEquals(4, (int) mergedMap.get(1));

        assertNotNull(mergedMap.get(2));
        assertEquals(5, (int) mergedMap.get(2));

        assertNotNull(mergedMap.get(3));
        assertEquals(6, (int) mergedMap.get(3));
    }

    //------------------------------------------------------------------------------------------------------------------ mergeWordMaps

    @Test
    public void mergeWordMapsTest_1() {
        Map<String, HashMap<String, Integer>> map1 = new HashMap<>();
        map1.put("en", new HashMap<>());
        map1.put("de", new HashMap<>());
        map1.put("es", new HashMap<>());

        map1.get("en").put("test1", 1);
        map1.get("de").put("test2", 2);
        map1.get("es").put("test3", 3);

        Map<String, HashMap<String, Integer>> map2 = new HashMap<>();
        map2.put("ko", new HashMap<>());
        map2.put("ja", new HashMap<>());
        map2.put("pt", new HashMap<>());

        map2.get("ko").put("test4", 4);
        map2.get("ja").put("test5", 5);
        map2.get("pt").put("test6", 6);

        Map<String, HashMap<String, Integer>> mergedMap = twitterStatusStatisticsMerger.mergeWordMaps(map1, map2);

        assertNotNull(mergedMap.get("en"));
        assertNotNull(mergedMap.get("en").get("test1"));
        assertEquals(1, (int) mergedMap.get("en").get("test1"));

        assertNotNull(mergedMap.get("de"));
        assertNotNull(mergedMap.get("de").get("test2"));
        assertEquals(2, (int) mergedMap.get("de").get("test2"));

        assertNotNull(mergedMap.get("es"));
        assertNotNull(mergedMap.get("es").get("test3"));
        assertEquals(3, (int) mergedMap.get("es").get("test3"));

        assertNotNull(mergedMap.get("ko"));
        assertNotNull(mergedMap.get("ko").get("test4"));
        assertEquals(4, (int) mergedMap.get("ko").get("test4"));

        assertNotNull(mergedMap.get("ja"));
        assertNotNull(mergedMap.get("ja").get("test5"));
        assertEquals(5, (int) mergedMap.get("ja").get("test5"));

        assertNotNull(mergedMap.get("pt"));
        assertNotNull(mergedMap.get("pt").get("test6"));
        assertEquals(6, (int) mergedMap.get("pt").get("test6"));
    }

    @Test
    public void mergeWordMapsTest_2() {
        Map<String, HashMap<String, Integer>> map1 = new HashMap<>();
        map1.put("en", new HashMap<>());
        map1.put("de", new HashMap<>());
        map1.put("es", new HashMap<>());

        map1.get("en").put("test1", 1);
        map1.get("de").put("test2", 2);
        map1.get("es").put("test3", 3);

        Map<String, HashMap<String, Integer>> map2 = new HashMap<>();
        map2.put("en", new HashMap<>());
        map2.put("de", new HashMap<>());
        map2.put("es", new HashMap<>());

        map2.get("en").put("test4", 4);
        map2.get("de").put("test5", 5);
        map2.get("es").put("test6", 6);

        Map<String, HashMap<String, Integer>> mergedMap = twitterStatusStatisticsMerger.mergeWordMaps(map1, map2);

        assertNotNull(mergedMap.get("en"));
        assertNotNull(mergedMap.get("en").get("test1"));
        assertEquals(1, (int) mergedMap.get("en").get("test1"));
        assertNotNull(mergedMap.get("en").get("test4"));
        assertEquals(4, (int) mergedMap.get("en").get("test4"));

        assertNotNull(mergedMap.get("de"));
        assertNotNull(mergedMap.get("de").get("test2"));
        assertEquals(2, (int) mergedMap.get("de").get("test2"));
        assertNotNull(mergedMap.get("de").get("test5"));
        assertEquals(5, (int) mergedMap.get("de").get("test5"));

        assertNotNull(mergedMap.get("es"));
        assertNotNull(mergedMap.get("es").get("test3"));
        assertEquals(3, (int) mergedMap.get("es").get("test3"));
        assertNotNull(mergedMap.get("es").get("test6"));
        assertEquals(6, (int) mergedMap.get("es").get("test6"));
    }

    @Test
    public void mergeWordMapsTest_3() {
        Map<String, HashMap<String, Integer>> map1 = new HashMap<>();
        map1.put("en", new HashMap<>());
        map1.put("de", new HashMap<>());
        map1.put("es", new HashMap<>());

        map1.get("en").put("test1", 1);
        map1.get("de").put("test2", 2);
        map1.get("es").put("test3", 3);

        Map<String, HashMap<String, Integer>> map2 = new HashMap<>();
        map2.put("en", new HashMap<>());
        map2.put("de", new HashMap<>());
        map2.put("es", new HashMap<>());

        map2.get("en").put("test1", 4);
        map2.get("de").put("test2", 5);
        map2.get("es").put("test3", 6);

        Map<String, HashMap<String, Integer>> mergedMap = twitterStatusStatisticsMerger.mergeWordMaps(map1, map2);

        assertNotNull(mergedMap.get("en"));
        assertNotNull(mergedMap.get("en").get("test1"));
        assertEquals(5, (int) mergedMap.get("en").get("test1"));

        assertNotNull(mergedMap.get("de"));
        assertNotNull(mergedMap.get("de").get("test2"));
        assertEquals(7, (int) mergedMap.get("de").get("test2"));

        assertNotNull(mergedMap.get("es"));
        assertNotNull(mergedMap.get("es").get("test3"));
        assertEquals(9, (int) mergedMap.get("es").get("test3"));
    }
}