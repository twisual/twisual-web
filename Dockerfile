FROM frolvlad/alpine-oraclejdk8:slim
MAINTAINER Filip Szuberski "filip.szuberski@gmail.com"
VOLUME /var/log
ADD build/libs/twisual-web.jar /root/twisual-web.jar
ARG PROFILE
ENV JAVA_OPTS="-Xmx4g -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=$PROFILE"
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /root/twisual-web.jar" ]